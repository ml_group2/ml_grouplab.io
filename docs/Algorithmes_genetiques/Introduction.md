# Algorithmes génétiques

Les algorithmes génétiques utilisent la théorie de **Darwin** sur l’évolution des espèces.  
Elle repose sur trois principes : le principe de **variation**, le principe d'**adaptation** et le principe d'**hérédité**.

> **Le principe de variation :** Chaque individu au sein d’une population est unique. Ces différences, plus ou moins importantes, vont être décisives dans le processus de sélection.

> **Le principe d'adaptation :** Les individus les plus adaptés à leur environnement atteignent plus facilement l'âge adulte. Ceux ayant une meilleure capacité de survie pourront donc se reproduire davantage.

> **Le principe d’hérédité :** Les caractéristiques des individus doivent être héréditaires pour pouvoir être transmises à leur descendance. Ce mécanisme permettra de faire évoluer l’espèce pour partager les caractéristiques avantageuse à sa survie.


Ce paradigme, associé avec la terminologie de la génétique, nous permet d’exploiter les algorithmes génétiques : Nous retrouvons les notions de Population, d’Individu, de Chromosome et de Gène.

-   **La population** est l’ensemble des solutions envisageables.
-   **L’individu** représente une solution.
-   **Le Chromosome** est une composante de la solution.
-   **Le Gène** est une caractéristique, une particularité.



# Opérateurs d'évolution

Il y a trois opérateurs d'évolution dans les algorithmes génétiques :

-   **La sélection :** Choix des individus les mieux adaptés.
-   **Le croisement :** Mélange par la reproduction des particularités des individus choisis.
-   **La mutation :** Altération aléatoire des particularités d'un individu.

### Sélection

La sélection consiste à choisir les individus les mieux adaptés afin d'avoir une population de solution la plus proche de converger vers l'optimum global.  
Cet opérateur est l'application du _principe d'adaptation_ de la théorie de Darwin.

Il existe plusieurs techniques de sélection. Voici les principales utilisées :

-   **Sélection par rang :** Cette technique de sélection choisit toujours les individus possédant les meilleurs scores d'adaptation.
-   **Probabilité de sélection proportionnelle à l'adaptation :** Technique de la roulette ou roue de la fortune, pour chaque individu, la probabilité d'être sélectionné est proportionnelle à son adaptation au problème.
-   **Sélection par tournoi :** Cette technique utilise la sélection proportionnelle sur des paires d'individus, puis choisit parmi ces paires l'individu qui a le meilleur score d'adaptation.
-   **Sélection uniforme :** La sélection se fait aléatoirement, uniformément et sans intervention de la valeur d'adaptation.

Une fois la sélection effectuée on réalise un croisement entre deux chromosomes parmi la population restante.


### Croisement

Le croisement, ou enjambement, crossing-over, est le résultat obtenue lorsque deux chromosomes partage leurs particularités.  
Celui-ci permet le brassage génétique de la population et l'application du _principe d’hérédité_ de la théorie de Darwin.

Il existe deux méthodes de croissement : simple ou double enjambement.

-   Le simple enjambement consiste à fusionner les particularités de deux individus à partir d'un pivot, afin d'obtenir un ou deux enfants. (On coupe en 2 les individus)
- Le double enjambement repose sur le même principe, sauf qu'il y a deux pivots. (On coupe en 3 les individus)

On réalise ensuite une mutation sur les enfants obtenues lors du croisement.


### Mutation

La mutation consiste à altérer un gène dans un chromosome selon un facteur de mutation. Ce facteur est la probabilité qu'une mutation soit effectuée sur un individu.  
Cet opérateur est l'application du _principe de variation_ de la théorie de Darwin et permet, par la même occassion, d'éviter une convergence prématurée de l'algorithme vers un extremum local.


Avec ces trois opérateurs d'évolution, nous pouvons appliquer les algorithmes génétiques.



# Algorithme

Voici le fonctionnement des algorithmes génétiques :

<figure markdown>
  ![algo genetique](images/algo_genetique_schema.png){ width="300" }
  <figcaption>Algorithme génétique</figcaption>
</figure>


-   La génèse est l'étape de la création d'une population aléatoire. C'est le point de départ de notre algorithme

-   L'évaluation est l'analyse des individus pour analyser si une solution est disponible. Pour ceci, nous utilisons un fonction de coût, ou d'erreur, afin de définir le score d'adaptation des individus lors du processus de sélection.

-   Nous effectuons une boucle tant que l'évaluation estime que la solution n'est pas optimale.