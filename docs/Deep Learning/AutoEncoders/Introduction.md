

## Définition 
Soit une variable aléatoire $X$ à valeurs dans $R^n$. Un réseau auto-encodeur modélise une fonction $H$ telle que : 
$$||H(x) − x|| ≤ ε$$

C'est un apprentissage **non supervisé**. Les autoencodeurs fonctionnent en compressant l'entrée dans une représentation de l'espace latent, puis en reconstruisant la sortie à partir de cette représentation.

L’auto-encodeur se décompose en deux parties : 
- Un **encodeur**, c'est la partie du réseau qui comprime l'entrée dans une représentation de l'espace latent. Elle peut être représentée par une fonction d'encodage $h=f(x)$ de $E$ : $R^n → R^d$  (modèle de reconnaissance)

- Un **décodeur**, c'est la partie qui vise à reconstruire l'entrée à partir de la représentation de l'espace latent. Elle peut être représentée par une fonction de décodage $r=g(h)$ de $D$ : $R^d → R^n$  (modèle génératif)


<figure markdown>
  ![](images/autoencodeurRep.png){ width="300" }
</figure>

## Configurations d'Autoencodeurs

**1. Undercomplete**


<figure markdown>
  ![](images/autoencodeur1.png){ width="300" }
</figure>

L'objectif d'un auto-codeur sous-complet est de capturer les caractéristiques les plus importantes présentes dans les données. Les auto-codeurs sous-complets ont une plus petite dimension pour la couche cachée par rapport à la couche d'entrée. Cela permet d'obtenir des caractéristiques importantes à partir des données. Il minimise la fonction de perte en pénalisant le g(f(x)) pour être différent de l'entrée x.  

- L'encodeur doit trouver une projection vers un espace de plus petite dimension
- Si f, g sont linéaire : proche de PCA (exactement si $f=U^T$ , $g = U$, $U^T U=I$)
- Si f, g sont non-linéaires, projections plus puissantes

> **Avantage** : les auto-codeurs **undercomplete** n'ont pas besoin de régularisation car ils maximisent la probabilité des données plutôt que de copier l'entrée sur la sortie.

> **Inconvénient** : l'utilisation d'un modèle surparamétré en raison d'un manque de données d'apprentissage suffisantes peut créer un surajustement.


**2. Overcomplete**


<figure markdown>
  ![](images/autoencodeur2.png){ width="300" }
</figure>

- Sera inutile sans régularisation -> copie de x dans h


## Applications

- Réduction de dimension pour la classification (généralisation)
- Data augmentation : [[Générer des ECG avec des variational AE]]
- Débruitage des données
- Permet de combiner non-supervisé avec supervisé : Semi-supervisé!

> Les autoencodeurs ne sont pas très performants pour la compression d'images. Comme l'auto-codeur est formé sur un ensemble de données donné, il obtiendra des résultats de compression raisonnables sur des données similaires à l'ensemble de formation utilisé, mais sera un mauvais compresseur d'image à usage général. Les auto-codeurs sont entraînés à préserver autant d'informations que possible lorsqu'une entrée passe par le codeur puis par le décodeur, mais ils sont également entraînés à faire en sorte que la nouvelle représentation ait diverses propriétés agréables.  

Avec des contraintes de dimensionnalité et de sparsité appropriées, les autoencodeurs peuvent apprendre des projections de données qui sont plus intéressantes que l'ACP ou d'autres techniques de base.


## Familles auto-encodeurs

#### 1. Sparse AE

L'auto-codeur clairsemé applique une contrainte sur l'activation des unités cachées afin d'éviter un overfitting et d'améliorer la robustesse. Elle oblige le modèle à n'activer qu'un petit nombre d'unités cachées en même temps, ou en d'autres termes, un neurone caché doit être inactivé la plupart du temps.

Pour toute observation donnée, nous encouragerons notre réseau à apprendre un codage et un décodage qui ne repose que sur l'activation d'un petit nombre de neurones. Il convient de noter qu'il s'agit d'une approche différente de la régularisation, car nous régularisons normalement les poids d'un réseau, et non les activations.  
  
Un _generic sparse autoencoder_ est visualisé ci-dessous, où l'opacité d'un nœud correspond au niveau d'activation. Il est important de noter que les nœuds individuels d'un modèle entraîné qui s'activent dépendent des données, différentes entrées entraîneront l'activation de différents nœuds dans le réseau.  


<figure markdown>
  ![](images/sparse_AE_1.png){ width="300" }
</figure>

Alors qu'un auto-codeur sous-complet utilisera l'ensemble du réseau pour chaque observation, un auto-codeur clairsemé sera obligé d'activer sélectivement des régions du réseau en fonction des données d'entrée. Par conséquent, nous avons limité la capacité du réseau à mémoriser les données d'entrée sans limiter la capacité du réseau à extraire des caractéristiques des données. Cela nous permet de considérer séparément la représentation de l'état latent et la régularisation du réseau, de sorte que nous pouvons choisir une représentation de l'état latent (c'est-à-dire la dimension de l'encodage) en fonction de ce qui est logique compte tenu du contexte des données, tout en imposant une régularisation par la contrainte de sparsité.  

La fraction d'activation du j-ième neurone de la l-ième couche cachée est notée $\hat\rho_j^{(l)}$ et est censée être plus petite que $\rho$ qui est la paramètre de sparsité, et souvent fixé à 0.05.

La contrainte de sparsité est obtenue en ajoutant un terme de pénalité dans la fonction de perte. La KL-divergence notée $D_{KL}$ mesure la différence entre deux distributions de Bernoulli, l'une avec une moyenne de $\rho$ et l'autre avec une moyenne de $\hat\rho_j^{(l)}$. L'hyper-paramètre $\beta$ controle la force de la pénalité à appliquer sur la perte.

$$L_{sparseAE}=L(\theta)+\beta\sum_{l=1}^L\sum_{j=1}^{s_l}D_{KL}(\rho||\hat\rho_j^{(l)})$$
$$=L(\theta)+\beta\sum_{l=1}^L\sum_{j=1}^{s_l}\rho log\frac{\rho}{\hat\rho_j^{(l)}}+(1-\rho)log\frac{1-\rho}{1-\hat\rho_j^{(l)}}$$


#### 2. Denoising AE

Les auto-codeurs de débruitage (Denoising AE) créent une copie corrompue de l'entrée en introduisant du bruit. Cela permet d'éviter que les auto-codeurs ne copient l'entrée vers la sortie sans apprendre les caractéristiques des données. Ces auto-codeurs prennent une entrée partiellement corrompue pendant l'apprentissage pour récupérer l'entrée originale non déformée. Le modèle apprend un champ vectoriel pour cartographier les données d'entrée vers un collecteur de dimension inférieure qui décrit les données naturelles afin d'annuler le bruit ajouté.  

Cette conception est motivée par le fait que les humains peuvent facilement reconnaître un objet ou une scène même si la vue est partiellement occultée ou corrompue. Pour "réparer" l'entrée partiellement détruite, l'autoencodeur de débruitage doit découvrir et capturer la relation entre les dimensions de l'entrée afin de déduire les pièces manquantes.

> **Inconvénients**
> - Pour entraîner un auto-codeur à débruiter les données, il est nécessaire d'effectuer un mappage stochastique préliminaire afin de corrompre les données et de les utiliser comme entrée.
> - Ce modèle n'est pas capable de développer une cartographie qui mémorise les données d'entraînement car notre entrée et la sortie cible ne sont plus les mêmes. 


<figure markdown>
  ![](images/denoising_AE_1.png){ width="300" }
</figure>


#### 3. Deep AE

Les auto-codeurs profonds se composent de deux réseaux de croyance (Deep belief network) profonds identiques, un réseau pour l'encodage et un autre pour le décodage. Typiquement, les auto-codeurs profonds ont 4 à 5 couches pour l'encodage et les 4 à 5 couches suivantes pour le décodage. Nous utilisons un pré-entraînement non supervisé couche par couche pour ce modèle. Les couches sont des **Machines de Boltzmann** restreintes qui sont les blocs de construction des réseaux de croyance profonds. En traitant l'ensemble de données de référence MNIST, un autoencodeur profond utiliserait des transformations binaires après chaque RBM (Machines de Boltzmann). Les auto-codeurs profonds sont utiles pour la classification de documents suivant leur sujet, ou la modélisation statistique de sujets abstraits distribués dans une collection de documents. Ils sont également capables de compresser des images en vecteurs de 30 nombres.  
  
> **Inconvénient**
> Risque d'overfitting, car il y a plus de paramètres que de données d'entrée.


<figure markdown>
  ![](images/Deep_AE.png){ width="300" }
</figure>

#### 4. Contractive AE

L'objectif d'un auto-codeur contractif (contractive AE) est d'obtenir une représentation apprise robuste qui soit moins sensible aux petites variations des données. La robustesse de la représentation pour les données est obtenue en appliquant un terme de pénalité à la fonction de perte. L'auto-codeur contractif est une autre technique de régularisation, tout comme les **Sparse** et **Denoising** Auto-encodeurs. Cependant, ce régularisateur correspond à la norme de Frobenius de la matrice jacobienne des activations de l'encodeur par rapport à l'entrée. La norme de Frobenius de la matrice jacobienne de la couche cachée est calculée par rapport à l'entrée et correspond essentiellement à la somme des carrés de tous les éléments.  

> Les auto-codeurs de débruitage font en sorte que la fonction de reconstruction (c'est-à-dire le **décodeur**) résiste à des petites perturbations de l'entrée mais de taille ﬁnie, tandis que les auto-codeurs contractifs font en sorte que la fonction d'extraction de caractéristiques (c'est-à-dire l'**encodeur**) résiste à des perturbations infinitésimales de l'entrée

Mathématiquement, nous pouvons concevoir notre terme de perte de régularisation comme la norme de **Frobenius** $||A||_f$ au carré de la matrice Jacobienne $J$ pour les activations de la couche cachée par rapport aux observations d'entrée. Une norme de Frobenius est essentiellement une norme L2 pour une matrice et la matrice jacobienne représente simplement toutes les dérivées partielles de premier ordre d'une fonction à valeur vectorielle.

On a pour $m$ observations et $n$ couches cachées: 

$$||A||_f=\sqrt{\sum_{i=1}^m\sum_{j=1}^n|a_{ij}|^2}$$

$$J=\left[\begin{array}{ccc}
\dfrac{\partial a_{1}^{(h)}(\mathbf{x})}{\partial x_{1}} & \cdots & \dfrac{\partial a_{1}^{(h)}(\mathbf{x})}{\partial x_{m}} \\
\vdots & \ddots & \vdots \\
\dfrac{\partial a_{n}^{(h)}(\mathbf{x})}{\partial x_{1}} & \cdots & \dfrac{\partial a_{n}^{(h)}(\mathbf{x})}{\partial x_{m}}
\end{array}\right]$$

La fonction cout peut alors s'écrire:

$$L(x, \hat x)+\lambda \sum_i||\nabla_xa_i^{(h)}(x)||^2$$

Avec $\nabla_xa_i^{(h)}(x)$ qui définit le champ de gradient des activations de notre couche cachée par rapport à l'entrée $x$ sommé sur tous les $i$ exemples d'apprentissage.



<figure markdown>
  ![](images/Contractive_AE.png){ width="300" }
</figure>

> **Avantages**
> - L'auto-codeur contractif est un meilleur choix que l'auto-codeur de débruitage pour apprendre l'extraction de caractéristiques utiles.
> - Ce modèle apprend un codage dans lequel des entrées similaires ont des codages similaires. Par conséquent, nous forçons le modèle à apprendre comment contracter un voisinage d'entrées en un plus petit voisinage de sorties.


#### 5. Variational AE

L'idée de l'auto-codeur variationnel est en fait moins similaire à tous les autres modèles auto-codeurs, mais profondément ancrée dans les méthodes des modèles bayésiens variationnels et graphiques. Au lieu de transformer l'entrée en un vecteur fixe, nous voulons la transformer en une distribution. 

Les modèles d'autoencodeurs variationnels font des hypothèses fortes concernant la distribution des variables latentes. Ils utilisent une approche variationnelle pour l'apprentissage de la représentation latente, ce qui entraîne une composante de perte supplémentaire et un estimateur spécifique pour l'algorithme d'apprentissage appelé estimateur de Bayes variationnel à gradient stochastique. Il suppose que les données sont générées par un modèle graphique dirigé et que l'encodeur apprend une approximation de la distribution postérieure où Ф et θ désignent respectivement les paramètres de l'encodeur (modèle de reconnaissance) et du décodeur (modèle génératif). La distribution de probabilité du vecteur latent d'un auto-codeur variationnel correspond généralement à celle des données d'apprentissage de manière beaucoup plus proche qu'un auto-codeur standard.  

Plus précisément, il s'agit d'un auto-codeur qui apprend un modèle de variable latente pour ses données d'entrée. Ainsi, au lieu de laisser votre réseau neuronal apprendre une fonction arbitraire, vous apprenez les paramètres d'une distribution de probabilité modélisant vos données. Si vous échantillonnez des points de cette distribution, vous pouvez générer de nouveaux échantillons de données d'entrée : un VAE est un "modèle génératif".  

**Fonctionnement** :
Tout d'abord, un réseau encodeur transforme les échantillons d'entrée $x$ en deux paramètres dans un espace latent, que nous noterons **z_mean** et **z_log_sigma**. 


<figure markdown>
  ![](images/VAE_separate_latent_space.png){ width="300" }
</figure>

Ensuite, nous échantillonnons aléatoirement des points similaires $z$ à partir de la distribution normale latente qui est supposée générer les données, via $z = z\_mean + exp(z\_log\_sigma) * \epsilon$, où $\epsilon$ est un tenseur normal aléatoire. 
Enfin, un réseau de décodage renvoie ces points d'espace latent aux données d'entrée originales.  
Les paramètres du modèle sont formés via deux fonctions de perte : une perte de reconstruction forçant les échantillons décodés à correspondre aux entrées initiales (comme dans nos autoencodeurs précédents), et la divergence $KL$ entre la distribution latente apprise et la distribution antérieure, agissant comme un terme de régularisation (il faut que la distribution reste proche de la distribution Normal(0, 1)). Vous pouvez en fait vous débarrasser entièrement de ce dernier terme, bien qu'il contribue à l'apprentissage d'espaces latents bien formés et à la réduction de l'adaptation excessive aux données d'apprentissage.  



<figure markdown>
  ![](images/VAE.png){ width="300" }
</figure>

La bakpropagation est effectuée grâce à une reparamétrisation au niveau de la couche de "sampling" qui est la couche juste après les deux couches de moyennes et d'écart types. 

#### 6. Convolutional AE

Dans leur formulation traditionnelle, les auto-codeurs ne tiennent pas compte du fait qu'un signal peut être considéré comme une somme d'autres signaux. Les auto-codeurs convolutifs utilisent l'opérateur de convolution pour exploiter cette observation. Ils apprennent à coder l'entrée dans un ensemble de signaux simples et essaient ensuite de reconstruire l'entrée à partir de ceux-ci, de modifier la géométrie ou la réflectance de l'image. Ils constituent l'état de l'art pour l'apprentissage non supervisé des filtres convolutifs. Une fois que ces filtres ont été appris, ils peuvent être appliqués à n'importe quelle entrée afin d'en extraire des caractéristiques. Ces caractéristiques peuvent ensuite être utilisées pour effectuer toute tâche nécessitant une représentation compacte de l'entrée, comme la classification.  

> **Avantages**
> - En raison de leur nature convolutive, ils s'adaptent bien aux images de taille réaliste et de haute dimension.
> - Peut supprimer le bruit de l'image ou reconstituer les parties manquantes.

> **Inconvénient**
> La reconstruction de l'image d'entrée est souvent floue et de moindre qualité en raison de la compression au cours de laquelle des informations sont perdues.


<figure markdown>
  ![](images/Conv_AE.png){ width="300" }
</figure>


## Ressources

- [Apprentissage & Reconnaissance des Formes: Auto-encodeurs & modèle génératifs (GANs)](https://blesaux.github.io/courses/IOGS_ARF_2019_App_09_AE_GANs.pdf)

- [Livres Autoencoder](https://www.deeplearningbook.org/contents/autoencoders.html)

- [Les AE](https://iq.opengenus.org/types-of-autoencoder/)

- [Diiferent types of AE](https://medium.datadriveninvestor.com/deep-learning-different-types-of-autoencoders-41d4fa5f7570)

