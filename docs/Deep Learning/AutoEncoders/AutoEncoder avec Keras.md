

```python
import keras
from keras import layers
from keras.datasets import mnist
import numpy as np
import matplotlib.pyplot as plt
from keras.callbacks import TensorBoard
from keras import regularizers
from keras import backend as K
```


### Modèle simple "fully connected"

```python
# We will normalize all values between 0 and 1 and we will flatten the 28x28 images into vectors of size 784.
(x_train, _), (x_test, _) = mnist.load_data()
x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = x_train.reshape((len(x_train),np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
```

```python
def basic_model():
	# This is the size of our encoded representations
	encoding_dim = 32 # 32 floats -> compression of factor 24.5, assuming the input is 784 floats
	# This is our input image
	input_img = keras.Input(shape=(784,))
	# "encoded" is the encoded representation of the input
	encoded = layers.Dense(encoding_dim, activation='relu')(input_img)
	# "decoded" is the lossy reconstruction of the input
	decoded = layers.Dense(784, activation='sigmoid')(encoded)
	# This model maps an input to its reconstruction
	autoencoder = keras.Model(input_img, decoded)
	# This model maps an input to its encoded representation
	encoder = keras.Model(input_img, encoded)
	# This is our encoded (32-dimensional) input
	encoded_input = keras.Input(shape=(encoding_dim,))
	# Retrieve the last layer of the autoencoder model
	decoder_layer = autoencoder.layers[-1]
	# Create the decoder model
	decoder = keras.Model(encoded_input, decoder_layer( encoded_input))

	return encoder, decoder, autoencoder

  
encoder, decoder, autoencoder = basic_model()
```

```python
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train,
epochs=50,
batch_size=256,
shuffle=True,
validation_data=(x_test, x_test))
```

```python
# Encode and decode some digits
# Note that we take them from the test set
decoded_imgs = autoencoder.predict(x_test)

n = 10 # How many digits we will display
plt.figure(figsize=(20, 4))

for i in range(n):
	# Display original
	ax = plt.subplot(2, n, i + 1)
	plt.imshow(x_test[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
	# Display reconstruction
	ax = plt.subplot(2, n, i + 1 + n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

plt.show()
```

<figure markdown>
  ![](first_model_keras_AE.png){ width="300" }
</figure>

Voici ce que nous obtenons. La ligne du haut représente les chiffres originaux, et la ligne du bas les chiffres reconstruits. Nous perdons pas mal de détails avec cette approche basique.



## Ajout de sparsité 

Dans l'exemple précédent, les représentations n'étaient contraintes que par la taille de la couche cachée (32). Dans une telle situation, ce qui se passe généralement est que la couche cachée apprend une approximation de l'ACP ([[Principal component analysis]]). Mais une autre façon de contraindre les représentations à être compactes est d'ajouter une contrainte de sparsité sur l'activité des représentations cachées, de sorte que moins d'unités "tirent" à un moment donné. Dans Keras, cela peut être fait en ajoutant un _activity_regularizer_ à notre couche Dense.

```python
def sparse_model():
	encoding_dim = 32
	input_img = keras.Input(shape=(784,))
	# Add a Dense layer with a L1 activity regularizer
	encoded = layers.Dense(encoding_dim, activation='relu',
	activity_regularizer=regularizers.l1(10e-5))(input_img)
	decoded = layers.Dense(784, activation='sigmoid')(encoded)
	autoencoder = keras.Model(input_img, decoded)

	return encoder, decoder, autoencoder

encoder, decoder, autoencoder = sparse_model()
```

```python
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train, epochs=50, batch_size=256, shuffle=True, validation_data=(x_test, x_test))
```

```python
# Encode and decode some digits
# Note that we take them from the *test* set
decoded_imgs = autoencoder.predict(x_test)

n = 10 # How many digits we will display

plt.figure(figsize=(20, 4))

for i in range(n):
	# Display original
	ax = plt.subplot(2, n, i + 1)
	plt.imshow(x_test[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
	# Display reconstruction
	ax = plt.subplot(2, n, i + 1 + n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
plt.show()
```

<figure markdown>
  ![](images/sparse_model_keras_AE.png){ width="300" }
</figure>

Ils ressemblent beaucoup au modèle précédent, la seule différence significative étant la dispersion des représentations encodées. `encoded_imgs.mean()` donne une valeur de 3,33 (sur nos 10 000 images de test), alors qu'avec le modèle précédent, la même quantité était de 7,30. Notre nouveau modèle produit donc des représentations codées deux fois plus éparses.



## Deep model

Nous ne devons pas nous limiter à une seule couche comme encodeur ou décodeur, nous pourrions plutôt utiliser une pile de couches :

```python
def deep_model():
	input_img = keras.Input(shape=(784,))
	encoded = layers.Dense(128, activation='relu')(input_img)
	encoded = layers.Dense(64, activation='relu')(encoded)
	encoded = layers.Dense(32, activation='relu')(encoded)
	
	decoded = layers.Dense(64, activation='relu')(encoded)
	decoded = layers.Dense(128, activation='relu')(decoded)
	decoded = layers.Dense(784, activation='sigmoid')(decoded)
	autoencoder = keras.Model(input_img, decoded)
	return encoder, decoder, autoencoder

  

encoder, decoder, autoencoder = deep_model()
```

```python
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train,
epochs=50,
batch_size=256,
shuffle=True,
validation_data=(x_test, x_test))
```

```python
# Encode and decode some digits
# Note that we take them from the *test* set
decoded_imgs = autoencoder.predict(x_test)

n = 10 # How many digits we will display

plt.figure(figsize=(20, 4))

for i in range(n):
	# Display original
	ax = plt.subplot(2, n, i + 1)
	plt.imshow(x_test[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	  
	# Display reconstruction
	ax = plt.subplot(2, n, i + 1 + n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

plt.show()
```


<figure markdown>
  ![](images/deep_model_keras_AE.png){ width="300" }
</figure>

Les performances sont un peu meilleures que celles de nos modèles précédents. Nos chiffres reconstruits sont également un peu mieux.




## Convolutional modèle

Puisque nos entrées sont des images, il est logique d'utiliser des réseaux de neurones convolutionnels ([[CNN]]) comme codeurs et décodeurs. Dans la pratique, les auto-codeurs appliqués aux images sont toujours des auto-codeurs convolutifs, ils sont tout simplement beaucoup plus performants.  
Implémentons-en un. L'encodeur consistera en une pile de couches **Conv2D** et **MaxPooling2D** (le max pooling étant utilisé pour le sous-échantillonnage spatial), tandis que le décodeur consistera en une pile de couches **Conv2D** et **UpSampling2D**.  

```python
def convolutional_model():
	input_img = keras.Input(shape=(28, 28, 1))
	
	x = layers.Conv2D(16, (3, 3), activation='relu', padding='same')(input_img)
	x = layers.MaxPooling2D((2, 2), padding='same')(x)
	x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
	x = layers.MaxPooling2D((2, 2), padding='same')(x)
	x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
	encoded = layers.MaxPooling2D((2, 2), padding='same')(x)
	
	# at this point the representation is (4, 4, 8) i.e. 128-dimensional
	x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
	x = layers.UpSampling2D((2, 2))(x)
	x = layers.Conv2D(8, (3, 3), activation='relu', padding='same')(x)
	x = layers.UpSampling2D((2, 2))(x)
	x = layers.Conv2D(16, (3, 3), activation='relu')(x)
	x = layers.UpSampling2D((2, 2))(x)
	decoded = layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)
	
	autoencoder = keras.Model(input_img, decoded)

return autoencoder


autoencoder = convolutional_model()

(x_train, _), (x_test, _) = mnist.load_data()
x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = np.reshape(x_train, (len(x_train), 28, 28, 1))
x_test = np.reshape(x_test, (len(x_test), 28, 28, 1))
```

```python
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train,
epochs=25,
batch_size=256,
shuffle=True,
validation_data=(x_test, x_test))
```

```python
decoded_imgs = autoencoder.predict(x_test)

n = 10
plt.figure(figsize=(20, 4))
for i in range(1, n + 1):
	# Display original
	ax = plt.subplot(2, n, i)
	plt.imshow(x_test[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	  
	# Display reconstruction
	ax = plt.subplot(2, n, i + n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

plt.show()
```




## Variationnel modèle

```python
original_dim = 28 * 28
intermediate_dim = 64
latent_dim = 2

inputs = keras.Input(shape=(original_dim,))
h = layers.Dense(intermediate_dim, activation='relu')(inputs)
z_mean = layers.Dense(latent_dim)(h)
z_log_sigma = layers.Dense(latent_dim)(h)
```

Nous pouvons utiliser ces paramètres pour échantillonner de nouveaux points similaires dans l'espace latent :

```python
def sampling(args):
	z_mean, z_log_sigma = args
	epsilon = K.random_normal(shape=(K.shape(z_mean)[0], 
    latent_dim), mean=0., stddev=0.1)
return z_mean + K.exp(z_log_sigma) * epsilon


z = layers.Lambda(sampling)([z_mean, z_log_sigma])
```

Finally, we can map these sampled latent points back to reconstructed inputs:

```python
# Create encoder
encoder = keras.Model(inputs, [z_mean, z_log_sigma, z], name='encoder')

# Create decoder
latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
x = layers.Dense(intermediate_dim, activation='relu')(latent_inputs)
outputs = layers.Dense(original_dim, activation='sigmoid')(x)
decoder = keras.Model(latent_inputs, outputs, name='decoder')

# instantiate VAE model
outputs = decoder(encoder(inputs)[2])
vae = keras.Model(inputs, outputs, name='vae_mlp')
```

Ce que nous avons fait jusqu'à présent nous permet d'instancier 3 modèles :  
- un auto-codeur de bout en bout qui relie les entrées aux reconstructions  
- un encodeur reliant les entrées à l'espace latent  
- un générateur qui peut prendre des points sur l'espace latent et qui produira les échantillons reconstruits correspondants.  

Nous entraînons le modèle en utilisant le modèle de bout en bout, avec une fonction de perte personnalisée : la somme d'un terme de reconstruction et du terme de régularisation de divergence $KL$.  

```python
reconstruction_loss = keras.losses.binary_crossentropy(inputs, outputs)
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_sigma - K.square(z_mean) - K.exp(z_log_sigma)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
vae_loss = K.mean(reconstruction_loss + kl_loss)
vae.add_loss(vae_loss)
vae.compile(optimizer='adam')
```

```python
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

vae.fit(x_train, x_train,
epochs=100,
batch_size=32,
validation_data=(x_test, x_test))
```

```python
x_test_encoded = encoder.predict(x_test, batch_size=32)
```

Comme notre espace latent est bidimensionnel, il est possible d'effectuer quelques visualisations intéressantes à ce stade. L'une d'elles consiste à regarder les voisinages des différentes classes sur le plan 2D latent :

```python
plt.figure(figsize=(6, 6))
plt.scatter(x_test_encoded[0][:, 0], x_test_encoded[0][:, 1], c=y_test, cmap='jet')
plt.colorbar()
plt.show()
```


<figure markdown>
  ![](images/variational_AE_keras.png){ width="300" }
</figure>

Chacun de ces clusters colorés est une classe de chiffre. Les clusters proches sont des chiffres qui sont structurellement similaires (c'est-à-dire des chiffres qui partagent des informations dans l'espace latent).  
Le VAE étant un modèle génératif, nous pouvons également l'utiliser pour générer de nouveaux chiffres ! Ici, nous allons balayer le plan latent, en échantillonnant les points latents à intervalles réguliers, et en générant le chiffre correspondant à chacun de ces points.

```python
# Display a 2D manifold of the digits
n = 15 # figure with 15x15 digits
digit_size = 28
figure = np.zeros((digit_size * n, digit_size * n))
# We will sample n points within [-15, 15] standard deviations
grid_x = np.linspace(-15, 15, n)
grid_y = np.linspace(-15, 15, n)
  
for i, yi in enumerate(grid_x):
	for j, xi in enumerate(grid_y):
		z_sample = np.array([[xi, yi]])
		x_decoded = decoder.predict(z_sample)
		digit = x_decoded[0].reshape(digit_size, digit_size)
		figure[i * digit_size: (i + 1) * digit_size,
			j * digit_size: (j + 1) * digit_size] = digit
```

```python
plt.figure(figsize=(10, 10))
plt.imshow(figure)
plt.show()
```



<figure markdown>
  ![](images/Latent_Manifold_keras_AE.png){ width="300" }
</figure>

On obtient une visualisation du collecteur latent (Latent Manifold) qui "génère" les chiffres MNIST.  (un collecteur décrit la région de haute densité où se concentrent les données d'entrée)



## Débruitage d'image

Mettons notre autoencodeur convolutif au service d'un problème de débruitage d'images. C'est simple : nous allons entraîner l'auto-codeur à transformer des images de chiffres bruyantes en images de chiffres propres.
Voici comment nous allons générer des chiffres bruyants synthétiques : nous appliquons une matrice de bruit gaussien et découpons les images entre 0 et 1.

```python
(x_train, _), (x_test, _) = mnist.load_data()

x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
x_train = np.reshape(x_train, (len(x_train), 28, 28, 1))
x_test = np.reshape(x_test, (len(x_test), 28, 28, 1))

noise_factor = 0.5
x_train_noisy = x_train + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_train.shape)
x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape)

x_train_noisy = np.clip(x_train_noisy, 0., 1.)
x_test_noisy = np.clip(x_test_noisy, 0., 1.)
```

Voici à quoi ressemblent les chiffres bruités :

```python
n = 10
plt.figure(figsize=(20, 2))
for i in range(1, n + 1):
	ax = plt.subplot(1, n, i)
	plt.imshow(x_test_noisy[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	plt.show()
```


<figure markdown>
  ![](images/noised_images_keras_AE.png){ width="300" }
</figure>


Si vous louchez, vous pouvez encore les reconnaître, mais à peine. Notre auto-codeur peut-il apprendre à récupérer les chiffres d'origine ? C'est ce que nous allons découvrir.
Par rapport au précédent auto-codeur convolutif, afin d'améliorer la qualité de la reconstruction, nous allons utiliser un modèle légèrement différent avec plus de filtres par couche :

```python
def image_denoising_model():
	input_img = keras.Input(shape=(28, 28, 1))
	x = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(input_img)
	x = layers.MaxPooling2D((2, 2), padding='same')(x)
	x = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(x)
	encoded = layers.MaxPooling2D((2, 2), padding='same')(x)
	# At this point the representation is (7, 7, 32)
	x = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(encoded)
	x = layers.UpSampling2D((2, 2))(x)
	x = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(x)
	x = layers.UpSampling2D((2, 2))(x)
	decoded = layers.Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)
	autoencoder = keras.Model(input_img, decoded)

	return autoencoder
  

autoencoder = image_denoising_model()
```

```python
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
autoencoder.fit(x_train_noisy, x_train, epochs=3, batch_size=64, shuffle=True, validation_data=(x_test_noisy, x_test))
```

Voyons maintenant les résultats. En haut, les chiffres bruités sont envoyés au réseau, et en bas, les chiffres sont reconstruits par le réseau.

```python
decoded_imgs = autoencoder.predict(x_test)

n = 10
plt.figure(figsize=(20, 4))
for i in range(1, n + 1):
	# Display original
	ax = plt.subplot(2, n, i)
	plt.imshow(x_test_noisy[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
	# Display reconstruction
	ax = plt.subplot(2, n, i + n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

plt.show()
```


<figure markdown>
  ![](images/denoised_images_compare_keras_AE.png){ width="300" }
</figure>
