
**Article sur lequel se base cette note [[GenerateECGwithAutoEncodes.pdf|ici]]**

## Abstract
Nous proposons une méthode pour générer un signal d'électrocardiogramme (ECG) pour un cycle cardiaque en utilisant un [[Intelligence artificielle/Deep Learning/AutoEncoders/Introduction#5. Variational AE|Variational auto-encoder]]. En utilisant cette méthode, nous avons extrait un vecteur de 25 nouvelles caractéristiques, qui peuvent être interprétées dans de nombreux cas. L'ECG généré a une apparence assez naturelle. La faible valeur de la métrique de l'écart moyen maximal, $3.83 × 10^{-3}$ indique une bonne qualité de la génération de l'ECG. Les nouvelles caractéristiques extraites permettront d'améliorer la qualité des diagnostics automatiques de maladies cardiovasculaires. En outre, la génération de nouveaux ECG synthétiques nous permettra de résoudre le problème du manque d'ECG labellisés pour les utiliser dans l'apprentissage supervisé.  


## Introduction

Une première approche pour construire une bonne description de caractéristiques était d'utiliser les connaissances d'un expert. Les spécialistes d'un domaine particulier Les spécialistes d'un domaine particulier proposent diverses méthodes pour construire les descriptions de caractéristiques, qui sont ensuite testées pour résoudre des problèmes pratiques. 
Une autre approche pour construire une bonne description de caractéristiques est l'extraction automatique de caractéristiques (également appelée réduction de la dimensionnalité). 

Il existe de nombreuses méthodes d'extraction automatique de caractéristiques,  
comme l'analyse en composantes principales, l'analyse en composantes indépendantes, les graphes principaux et les manifolds, les méthodes à noyaux,  
les autoencodeurs, les embeddings, etc. 

Nous examinons ici une méthode d'extraction automatique de caractéristiques,  
appelée [[Intelligence artificielle/Deep Learning/AutoEncoders/Introduction#5. Variational AE|Variational autoencoder]] (VAE) pour le problème du traitement automatique des électrocardiogrammes (ECG).  
L'électrocardiogramme est un enregistrement de l'activité électrique du cœur, obtenue à l'aide d'électrodes placées sur le corps humain. L'électrocardiographie est l'une des méthodes les plus importantes en cardiologie. La représentation schématique de la partie principale de l'ECG est présentée dans la figure ci-dessous:

  <figure markdown>
  ![](images/ECG_main_parts.png){ width="300" }
  </figure>

_Représentation schématique des principales parties du signal ECG pour un
cycle cardiaque : ondes P, T, U et complexe QRS, composé des pics Q, R et S._

Un cycle cardiaque (le comportement du cœur entre le début d'un battement et le début du suivant) contient des ondes P, T, U et le complexe QRS composé des pics Q, R et S. La taille, la forme et la localisation de ces parties donnent de grandes informations diagnostiques sur le travail du cœur et sur la présence/absence de certaines maladies. Récemment, les méthodes d'apprentissage automatique (notamment l'apprentissage profond) sont largement utilisées pour l'analyse automatique de l'ECG. Les tâches d'application comprennent la segmentation de l'ECG, la détection des maladies, la détermination du stade du sommeil, l'identification humaine biométrique, le débruitage, etc. Une variété de méthodes classiques et nouvelles sont utilisées. Parmi elles, l'analyse discriminante [[Analyse discriminante linéaire (LDA)]], [[Intelligence artificielle/Machine Learning/Classification/Decision tree]], [[Intelligence artificielle/Machine Learning/Classification/Support Vector Machine]], les [[CNN]] et [[ANN]], les [[Reccurent neural network (RNN)]], les [[GAN]], les [[Intelligence artificielle/Deep Learning/AutoEncoders/Introduction|Autoencodeurs]], etc.  

De notre point de vue, les directions les plus intéressantes et fructueuses dans l'application des méthodes d'apprentissage profond à l'analyse de l'ECG sont la génération d'ECG synthétique et l'extraction automatique de nouvelles caractéristiques interprétables. 

Notre approche pour générer un ECG est basée sur le VAE. Nous proposons des architectures de réseaux neuronaux pour un encodeur et un décodeur afin de générer des ECG synthétiques et d'extraire de nouvelles caractéristiques. Les ECG synthétiques générés semblent assez naturels. Le principal avantage de notre travail est que nous proposons la méthode d'extraction de nouvelles caractéristiques. Nos expériences montrent que ces caractéristiques sont tout à fait interprétables. Ce fait nous permet d'espérer que l'utilisation de ces caractéristiques aidera à améliorer la qualité des diagnostics automatiques des maladies cardiovasculaires.  
En outre, la génération de nouveaux ECG synthétiques nous permettra de résoudre le problème du manque d'ECG étiquetés pour les utiliser dans des applications supervisées.  


## Algorithme

**A. Preprocessing**

Les ECG originaux sont des signaux de 10 secondes à 12 dérivations avec une  
fréquence de 500 Hz. Chaque signal est coupé en signaux de neuf secondes. En utilisant les algorithmes de segmentation décrits dans l'article [Deep Learning for ECG Segmentation](https://arxiv.org/abs/2001.04689) dont le code est disponible [ici](https://github.com/byschii/ecg-segmentation/blob/main/unet_for_ecg.ipynb), nous déterminons les débuts et les fins de toutes les ondes P et T et tous les pics R. Ensuite, nous faisons le pas en avant et en arrière  à partir du pic R à une distance égale. Ainsi, nous obtenons l'ensemble des cycles cardiaques, chacun d'une longueur de 400.  

**B. Encoder architecture**

L'encodeur est constitué d'une partie convolution (en haut sur l'image) et d'une partie fully connected (en bas sur l'image). 
La chaîne convolutive (en haut du circuit dans la figure 2) se compose de 4 blocs connectés en série, chacun d'entre eux étant constitué d'une couche de convolution, d'une couche de normalisation par lots, d'une fonction d'activation ReLU et d'une couche MaxPooling. Ensuite, nous avons une autre couche de convolution. À la sortie de ce bloc, nous avons 25 neurones.  
La chaîne entièrement connectée de l'encodeur (en bas du circuit de la figure 2) se compose de 3 couches entièrement connectées (denses), interconnectées par une normalisation par lots et des fonctions d'activation ReLU. À la sortie de la dernière couche entièrement connectée, nous avons 25 neurones.  


<figure markdown>
  ![](images/Encoder_Archi_ECG.png){ width="300" }
</figure>

Les sorties des chaînes convolutionnelles et entièrement connectées sont concaténées, ce qui nous donne un vecteur de longueur 50. En utilisant deux couches entièrement connectées, nous obtenons deux vecteurs de 25 dimensions qui s'interprètent comme un vecteur de moyennes et un vecteur de logarithmes des variances pour 25 distributions normales (ou pour une distribution normale à 25 dimensions avec une matrice de covariance diagonale). La sortie de l'encodeur est un vecteur de longueur 25, dans lequel chaque composante est échantillonnée à partir de ces normales avec des moyennes et des variances spécifiées.  
Nous allons interpréter ce vecteur à 25 dimensions comme un vecteur de nouvelles caractéristiques suffisantes pour décrire et restituer avec une faible erreur un cycle cardiaque.  

On utilise ici comme fonction cout, la distance de Kullback-Leibler:
$$D_{KL}(P||Q)=\int_xp*log(\frac{p}{q})d\mu$$

De ce fait, ces 25 nouveaux éléments sont de distribution normale. Dans cette distance, $\mu$ est une mesure quelconque sur $X$ pour laquelle il existe une fonction absolument continue par raport à $\mu$; $p=\frac{dP}{d\mu}$ et $q=\frac{dQ}{d\mu}$, ou $P$ est la distribution initiale et Q est la nouvelle distribution obtenue.

**C. Decoder architecture**

En entrée, le décodeur accepte le vecteur de 25 dimensions de caractéristiques. Ensuite, de la même manière que pour l'encodeur, le branchement en chaînes convolutionnelles et entièrement connectées se produit. 
La chaîne entièrement connectée (au bas du circuit de la figure 3) se compose de 4 blocs, chacun contenant une couche entièrement connectée (dense), une couche de normalisation par lots et la fonction d'activation ReLU.  
La chaîne convolutive (en haut du circuit de la figure 3) effectue une déconvolution. Elle se compose de 4 blocs constitués d'une couche convolutionnelle, d'une couche de normalisation par lot et d'une fonction d'activation ReLU, suivie d'une couche de suréchantillonnage.  

<figure markdown>
  ![](images/Decoder_Archi_ECG.png){ width="300" }
</figure>

Le résultat de la chaîne convolutive et de la chaîne entièrement connectée est de 400 neurones chacune. Ensuite, nous concaténons deux résultats, obtenant 800 neurones. En utilisant une couche dense, nous obtenons 400 neurones qui représentent l'ECG restauré. Comme fonction de perte pour la sortie de l'encodeur, nous utilisons l'[[Fonction cout#**Mean Squared Error (MSE)**|erreur quadratique moyenne]].  

## Résultats expérimentaux

Comme test d'entraînement, nous utilisons 2033 signaux ECG de 10 secondes de fréquence 500 Hz. Nous les traitons selon les principes décrits ci-dessus et entraînons notre réseau sur les 252636 cycles cardiaques obtenus. Des exemples de ces cycles cardiaques sont présentés dans la Figure 4 ci-dessous.


<figure markdown>
  ![](images/authentic_ECG.png){ width="300" }
</figure>

Après avoir entraîné le réseau, nous pouvons tester le décodeur en fournissant des nombres aléatoires (générés selon la distribution normale standard) à son entrée. Des exemples des résultats produits sont présentés dans la Figure 5 ci-dessous. Ces ECG générés de manière synthétique semblent tout à fait naturels.  


<figure markdown>
  ![](images/generated_ECG.png){ width="300" }
</figure>

De plus, pour évaluer nos résultats, nous avons calculé la métrique MMD (Maximum Mean Discrepancy) sur l'ensemble des 3000 ECG générés. La valeur du MMD est égale à 3,83 × 10-3 .  
Des résultats intéressants ont été obtenus en générant des ECG avec une caractéristique variable. Certains signaux ECG générés sont présentés à la figure 6 ci-dessous. Pour chaque test, 24 caractéristiques étaient fixes alors que la caractéristique restante changeait. Il a été possible de trouver un paramètre responsable, par exemple, de la hauteur de l'onde T, de la dépression de l'onde ST, etc. dépression de l'onde ST, etc.  


<figure markdown>
  ![](images/ECG_generated_parameters.png){ width="300" }
</figure>

Ainsi, dans de nombreux cas, les caractéristiques extraites peuvent être interprétées, ce qui confirme également la haute qualité de la description des caractéristiques construites.

## Conclusion

Dans cet article, nous avons proposé une architecture de réseau neuronal (autoencodeur variationnel) qui est utilisée pour générer un ECG correspondant à un seul cycle cardiaque. Notre méthode génère des ECG synthétiques d'apparence totalement naturelle, qui peuvent être utilisés pour augmenter les ensembles d'entrainement dans les problèmes d'apprentissage supervisé impliquant des ECG. De plus, notre méthode nous a permis d'extraire de nouvelles caractéristiques qui caractérisent avec précision l'ECG.  
Les expériences montrent que les caractéristiques extraites se prêtent généralement à une bonne interprétation. Nous prévoyons d'utiliser notre approche pour générer l'ECG entier, et pas seulement un cycle cardiaque. Nous utiliserons également les caractéristiques extraites pour améliorer la qualité du diagnostic automatique des maladies cardiovasculaires.  
