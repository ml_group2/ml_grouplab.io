L'idée principale derrière l'attention est qu'au lieu de produire un seul état caché pour la séquence d'entrée, l'encodeur produit un état caché à chaque étape auquel le décodeur peut accéder. Cependant, l'utilisation de tous les états en même temps créerait une entrée énorme pour le décodeur, donc un mécanisme est nécessaire pour prioriser les états à utiliser. 
C'est là qu'intervient l'attention: **elle permet au décodeur d'attribuer un poids différent, ou " attention ", à chacun des états du codeur à chaque étape du décodage**. Ce processus est illustré dans la figure ci-dessous, où le rôle de l'attention est montré pour la prédiction du troisième jeton de la séquence de sortie.  

![[encode_decoder_attention.png]]
_An encoder-decoder architecture with an attention mechanism for a pair of RNNs_


En se concentrant sur les tokens d'entrée les plus pertinents à chaque pas de temps, ces modèles basés sur l'attention sont capables d'apprendre des alignements non triviaux entre les mots d'une traduction générée et ceux d'une phrase source. Par exemple, la figure ci-dessous visualise les poids d'attention pour un modèle de traduction de l'anglais vers le français, où chaque pixel représente un poids. La figure montre comment le décodeur est capable d'aligner correctement les mots "zone" et "Area", qui sont ordonnés différemment dans les deux langues.  

![[matrix_translation_fr_en.png]]
_RNN encoder-decoder alignment of words in English and the generated translation in French_


Bien que l'attention ait permis de produire de bien meilleures traductions, l'utilisation de modèles récurrents pour l'encodeur et le décodeur présentait toujours un inconvénient majeur : les calculs sont intrinsèquement séquentiels et ne peuvent être parallélisés sur la séquence d'entrée.  

Avec le transformer, un nouveau paradigme de modélisation a été introduit : il s'agit de renoncer complètement à la récurrence et de s'appuyer entièrement sur une forme spéciale d'attention appelée auto-attention. L'idée de base est de permettre à l'attention d'opérer sur tous les états de la même couche du réseau neuronal. L'illustration ci-dessous montre que l'encodeur et le décodeur disposent de leurs propres mécanismes d'auto-attention, dont les sorties alimentent des réseaux neuronaux à action directe. Cette architecture peut être entrainée beaucoup plus rapidement que les modèles récurrents et a ouvert la voie à de nombreuses percées récentes dans le domaine du traitement automatique des langues.


![[encoder_decoder_original_transformer.png]]
_Encoder-decoder architecture of the original Transformer_


Dans l'article original du Transformer, le modèle de traduction a été entraîné à partir de zéro sur un grand corpus de paires de phrases dans différentes langues. Cependant, dans de nombreuses applications pratiques du NLP, nous n'avons pas accès à de grandes quantités de données textuelles étiquetées pour entraîner nos modèles. Il manquait une dernière pièce pour faire démarrer la révolution des transformateurs : l'apprentissage par transfert.


Voir [[Transfert Learning en NLP]]
