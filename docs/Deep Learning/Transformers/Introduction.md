
En 2017, des chercheurs de Google ont publié un article proposant une nouvelle architecture de réseaux neuronaux pour la modélisation de séquences. Baptisée Transformer, cette architecture a surpassé les réseaux neuronaux récurrents (RNN) sur des tâches de traduction automatique, tant en termes de qualité de traduction que de coût de d'entrainement.  
Parallèlement, une méthode efficace de transfert learning appelée **ULMFiT** a montré que l'entraînement de réseaux à mémoire à long terme (LSTM) sur un corpus très large et diversifié pouvait produire des classificateurs de texte de pointe avec peu de données étiquetées.
Ces avancées ont été les catalyseurs de deux des transformateurs les plus connus aujourd'hui : le **Generative Pretrained Transformer (GPT)** et le **Bidirectional Encoder Representations from Transformers (BERT)**. En combinant l'architecture du transformer avec l'apprentissage non supervisé, ces modèles ont supprimé la nécessité de former des architectures spécifiques à la tâche à partir de zéro et ont battu presque tous les repères dans le domaine du NLP de façon significative. Depuis la sortie de GPT et de BERT, un zoo de modèles Transformer a vu le jour, une chronologie des entrées les plus importantes est présentée ci-dessous:

![[transformers_history.png]]


### encoder-decoder  framework

Avant les transformers, les architectures récurrentes telles que les [[Long short term memory (LSTM)|LSTM]] étaient l'état de l'art en NLP. Ces architectures contiennent une boucle de rétroaction dans les connexions du réseau qui permet aux informations de se propager d'une étape à l'autre, ce qui les rend idéales pour la modélisation de données séquentielles comme le texte. Comme l'illustre la partie gauche de la figure ci-dessous, un RNN reçoit une entrée (qui peut être un mot ou un caractère), la fait passer dans le réseau et produit un vecteur appelé état caché. Dans le même temps, le modèle se renvoie des informations par le biais de la boucle de rétroaction, qu'il peut ensuite utiliser à l'étape suivante. On le voit plus clairement si l'on "déroule" la boucle comme indiqué sur la droite de la figure: le RNN transmet des informations sur son état à chaque étape à l'opération suivante de la séquence. Cela permet à un RNN de garder la trace des informations des étapes précédentes et de les utiliser pour ses prédictions de sortie.  


![[unrolling_an_rnn.png]]
_Unrolling an RNN in time_

Ces architectures ont été (et continuent d'être) largement utilisées pour les tâches de NLP, le traitement de la parole et les séries temporelles. Vous trouverez un excellent exposé de leurs capacités dans l'article du blog d'Andrej Karpathy intitulé "The Unreasonable Effectiveness of Recurrent Neural Networks".  
Les RNN ont joué un rôle important dans le développement des systèmes de traduction automatique, dont l'objectif est de traduire une séquence de mots d'une langue à une autre. Ce type de tâche est généralement abordé avec une architecture encodeur-décodeur ou séquence à séquence, qui convient bien aux situations où l'entrée et la sortie sont toutes deux des séquences de longueur arbitraire. Le travail de l'encodeur consiste à encoder l'information de la séquence d'entrée dans une représentation numérique, souvent appelée __dernier état caché__. Cet état est ensuite transmis au décodeur, qui génère la séquence de sortie.  
En général, les composants de l'encodeur et du décodeur peuvent être n'importe quel type d'architecture de réseau neuronal capable de modéliser des séquences. Ceci est illustré pour une paire de RNN dans la figure ci-dessous, où la phrase anglaise "Transformers are great !" est codée sous la forme d'un vecteur d'état caché qui est ensuite décodé pour produire la traduction allemande "Transformer sind grossartig !". Les mots d'entrée sont introduits séquentiellement dans l'encodeur et les mots de sortie sont générés un par un, de haut en bas.  
  
![[encoder_decoder_pair_RNN.png]]
_An encoder-decoder architecture with a pair of RNNs (in general, there are many more recurrent layers than those shown here)_


Bien qu'élégante dans sa simplicité, l'une des faiblesses de cette architecture est que l'état caché final de l'encodeur crée un goulot d'étranglement en matière d'information : il doit représenter la signification de toute la séquence d'entrée car c'est tout ce à quoi le décodeur a accès lorsqu'il génère la sortie. C'est particulièrement difficile pour les longues séquences, où l'information au début de la séquence peut être perdue dans le processus de compression de toute la séquence en une seule représentation fixe.  
Heureusement, il existe un moyen d'éliminer ce goulot d'étranglement en permettant au décodeur d'avoir accès à tous les états cachés du codeur. Le mécanisme général utilisé à cet effet s'appelle **l'attention** et constitue un élément clé de nombreuses architectures de réseaux neuronaux modernes. En comprenant comment l'attention a été développée pour les RNN, nous serons en mesure de comprendre l'un des principaux éléments constitutifs de l'architecture Transformer. 

Voir [[Le mécanisme d'attention]]




