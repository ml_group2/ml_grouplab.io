Dans le domaine de la vision par ordinateur, il est aujourd'hui courant d'utiliser le transfert learning pour former un réseau de neurones convolutifs comme ResNet à une tâche, puis de l'adapter ou de l'affiner à une nouvelle tâche. Cela permet au réseau d'utiliser les connaissances acquises lors de la tâche initiale. Sur le plan architectural, cela implique de diviser le modèle en un corps et une tête, la tête étant un réseau spécifique à la tâche. Au cours de l'entrainement, les poids du corps apprennent les caractéristiques générales du domaine source, et ces poids sont utilisés pour initialiser un nouveau modèle pour la nouvelle tâche. Par rapport à l'apprentissage supervisé traditionnel, cette approche produit généralement des modèles de haute qualité qui peuvent être formés beaucoup plus efficacement sur une variété de tâches en aval, et avec beaucoup moins de données étiquetées. La figure ci-dessout présente une comparaison de ces deux approches.


  ![[comparaison_transfertL_tradSupervisedL.png]]
_Comparison of traditional supervised learning (left) and transfer learning (right)_

En vision par ordinateur, les modèles sont d'abord entraînés sur des ensembles de données à grande échelle tels que Image-Net, qui contiennent des millions d'images. Ce processus, appelé pré-entraînement, a pour principal objectif d'enseigner aux modèles les caractéristiques de base des images, telles que les bords ou les couleurs. Ces modèles pré-entraînés peuvent ensuite être affinés sur une tâche en aval, telle que la classification des espèces de fleurs, avec un nombre relativement faible d'exemples étiquetés (généralement quelques centaines par classe). Les modèles affinés atteignent généralement une plus grande précision que les modèles supervisés formés à partir de zéro sur la même quantité de données étiquetées.  
  
Bien que l'apprentissage par transfert soit devenu l'approche standard en vision par ordinateur, pendant de nombreuses années, le processus de pré-entraînement analogue n'était pas clair pour le langage naturel. Par conséquent, les applications NLP nécessitaient généralement de grandes quantités de données étiquetées pour atteindre des performances élevées. Et même dans ce cas, ces performances n'étaient pas comparables à celles obtenues dans le domaine de la vision. 



En 2017 et 2018, plusieurs groupes de recherche ont proposé de nouvelles approches qui ont finalement fait fonctionner l'apprentissage par transfert pour le NLP. Cela a commencé par une idée des chercheurs d'OpenAI qui ont obtenu de fortes performances sur une tâche de classification de sentiments en utilisant des caractéristiques extraites d'un pré-entraînement non supervisé. Cela a été suivi par ULMFiT, qui a introduit un cadre général pour adapter des modèles LSTM pré-entraînés à diverses tâches.

Comme l'illustre la figure ci-dessous, ULMFiT comporte trois étapes principales :

**Pretraining**
L'objectif initial de l'entrainement est assez simple : prédire le mot suivant sur la base des mots précédents. Cette tâche est appelée _language modeling_. L'élégance de cette approche réside dans le fait qu'aucune donnée étiquetée n'est nécessaire et que l'on peut utiliser des textes disponibles en abondance à partir de sources telles que Wikipedia.

**Domain adaptation**
Une fois le modèle de langage pré-entraîné sur un corpus à grande échelle, l'étape suivante consiste à l'adapter au corpus du domaine (par exemple, de Wikipedia au corpus IMDb de critiques de films, comme dans la Figure ci-dessous). Cette étape utilise toujours la modélisation du langage, mais le modèle doit maintenant prédire le mot suivant dans le corpus cible.

**Fine-tuning**
Au cours de cette étape, le modèle linguistique est affiné à l'aide d'une couche de classification pour la tâche cible (par exemple, la classification du sentiment des critiques de films dans la figure ci-dessous).


![[ULMFiT.png]]
_ULMFiT process_


En introduisant un cadre viable pour la préformation et l'apprentissage par transfert en NLP, ULMFiT a fourni la pièce manquante pour faire décoller les transformateurs. En 2018, deux transformer ont été publiés, qui combinaient le self-attention et le transfert learning :

**GPT**
Utilise uniquement la partie décodeur de l'architecture Transformer, et la même approche de modélisation de la langue que ULMFiT. GPT a été pré-entraîné sur le BookCorpus, qui se compose de 7000 livres non publiés dans une variété de genres comme l'aventure, la fantaisie et la romance.

**BERT**
Utilise la partie encodeur de l'architecture Transformer, et une forme spéciale de modélisation du langage appelée modélisation du langage masqué. L'objectif de la modélisation du langage masqué est de prédire les mots masqués de façon aléatoire dans un texte. Par exemple, dans une phrase comme "J'ai regardé mon [MASK] et j'ai vu que [MASK] était en retard", le modèle doit prédire les candidats les plus probables pour les mots masqués qui sont désignés par [MASK]. BERT a été pré-entraîné sur le BookCorpus et le Wikipedia anglais.


GPT et BERT ont établi un nouvel état de l'art à travers une variété de benchmarks NLP et ont inauguré l'ère des transformateurs. 
Avec la sortie des Transformers, une API unifiée pour plus de 50 architectures a été progressivement construite. Cette bibliothèque a catalysé l'explosion de la recherche sur les transformers et s'est rapidement étendue aux praticiens du NLP, facilitant ainsi l'intégration de ces modèles dans de nombreuses applications réelles. C'est **Hugging Face**.

