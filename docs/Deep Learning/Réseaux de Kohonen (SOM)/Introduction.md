
Inspiré de l'organisation du cortex, les réseaux auto-organisés se distinguent par une connectivité locale. Ils sont surtout adaptés pour le traitement d'informations spatiales. Le modèle le plus connu de ce type est la carte auto-organisatrice de Kohonen appelée aussi carte auto-adaptative ou SOM pour _self organizing map_.

Ces réseaux utilisent des méthodes d'**apprentissage non-supervisées**. Ils peuvent être utilisé pour cartographier un espace réel ou encore étudier la répartition de données dans un espace de grandes dimensions comme dans le cas de problème de quantification vectorielle, de clusterisation ou de classification.

![[som.png]]
Deux exemples d'utilisations de cartes auto-organisées.

Exemple d'utilisation : résolution du problème du voyageur de commerce (Voir [[Autres|ressources]])

## Aperçu technique

L'article original publié par Teuvo Kohonen en 19981 consiste en une brève description magistrale de la technique. Il y est expliqué qu'une carte auto-organisatrice est décrite comme une grille (généralement bidimensionnelle) de nœuds, inspirée d'un réseau neuronal. L'idée du modèle, c'est-à-dire de l'observation du monde réel que la carte tente de représenter, est étroitement liée à la carte. L'objectif de cette technique est de représenter le modèle avec un nombre inférieur de dimensions, tout en maintenant les relations de similarité des nœuds qu'il contient.  
  
Pour capturer cette similarité, les nœuds de la carte sont organisés dans l'espace de manière à être d'autant plus proches qu'ils sont similaires les uns aux autres. C'est pourquoi les SOM constituent un excellent moyen de visualiser et d'organiser les données. Pour obtenir cette structure, on applique à la carte une opération de régression pour modifier la position des nœuds afin de mettre à jour les nœuds, un élément du modèle ($e$) à la fois. L'expression utilisée pour la régression est la suivante :  $$n_{t+1}=n_t+h(w_e).\Delta(e,n_t)$$
Cela implique que la position du nœud $n$ est mise à jour en ajoutant la distance qui le sépare de l'élément donné, multipliée par le facteur de voisinage du neurone gagnant, $w_e$. Le gagnant d'un élément est le nœud le plus similaire à celui-ci dans la carte, généralement mesuré par le nœud le plus proche en utilisant la distance euclidienne (bien qu'il soit possible d'utiliser une mesure de similarité différente si nécessaire).  
  
De l'autre côté, le voisinage est défini comme un noyau de type convolution pour la carte autour du gagnant. De cette manière, nous sommes en mesure de mettre à jour le gagnant et les neurones proches de l'élément, obtenant ainsi un résultat doux et proportionnel. La fonction est généralement définie comme une distribution gaussienne, mais d'autres implémentations sont également possibles. L'une d'entre elles mérite d'être mentionnée : le voisinage à bulles, qui met à jour les neurones qui se trouvent dans un rayon du gagnant (sur la base d'une fonction delta de Kronecker discrète), qui est la fonction de voisinage la plus simple possible.  
