
## Pourquoi utiliser des SOM ? 

Fondamentalement, les SOM se caractérisent par une mise en correspondance non linéaire, ordonnée et lisse des collecteurs de données d'entrée de haute dimension avec les éléments d'un tableau régulier de basse dimension. Après l'entraînement des neurones du SOM, nous obtenons une représentation de faible dimension de données d'entrée de haute dimension sans perturber la forme de la distribution des données et la relation entre chaque élément de données d'entrée. Les cartes auto-organisatrices diffèrent des autres ANN car elles appliquent un apprentissage non supervisé par rapport à l'apprentissage par correction d'erreurs (rétropropagation avec descente de gradient, etc.), et dans le sens où elles utilisent une fonction de voisinage pour préserver les propriétés topologiques de l'espace d'entrée. Grâce à sa simplicité, nous pouvons facilement expliquer et démontrer ses capacités. 


## Comment ça marche ?

![[SOM_ex1.png]]

Une illustration simple du processus d'apprentissage est donnée dans la figure ci-dessus et nous pouvons facilement comprendre la spécialité des SOMs à partir de cette représentation. Au départ, les données d'entrée (points bleus) occupent une distribution particulière dans l'espace 2D, et les valeurs des neurones (poids) non apprises (points rouges) sont distribuées de manière aléatoire dans une petite zone. Après avoir été modifiés et appris par les entrées, les neurones prennent la forme de la distribution des données d'entrée étape par étape au cours du processus d'apprentissage. En outre, chaque neurone devient une représentation d'un petit groupe de l'espace des données d'entrée. Par conséquent, dans cette démonstration, nous avons pu représenter 1000 points de données avec 100 neurones, en préservant la topologie des données d'entrée. Cela signifie que nous avons établi une relation entre les données à haute dimension et la représentation à basse dimension (carte). Pour les calculs et les prédictions ultérieurs, nous pouvons utiliser ces quelques valeurs de neurones pour représenter l'énorme espace de données d'entrée, ce qui rend les processus beaucoup plus rapides.  
