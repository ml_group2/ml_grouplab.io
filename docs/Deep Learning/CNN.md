
[[#Introduction]]
[[#Noyaux et filtres d’image]]
[[#Convolutions]]
[[#Couches de Pooling]]
[[#Transfert learning]]
[[#UpSampling]]
[[#Architectures]]
[[#Cheat sheet]]


# Introduction

Les CNN sont une architecture spécifique des réseaux de neurones qui sont extrêmement efficaces pour traiter les données d'images. Ce traitement fait partie de ce qu'on appelle la vision par ordinateur. Cela désigne l'utilisation de programmes informatiques pour traiter des données d'images. Les représentations qu'ils apprennent sont faciles à inspecter - les réseaux ConvNet sont l'opposé des boîtes noires !

On ajoute des couches denses à la fin du réseau pour faire la classification. Sans ça, on apprend juste les filtres. Le but est de trouver les filtres optimum. Pour utiliser un CNN en tant que classificateur, il faut ajouter un classifieur à la fin, avec un **flatten** et une méthode de classification (SVM, KNN, ...) avec une **softmax**. 


## Noyaux et filtres d’image

Exemple de filtre : 

![[filtreflou.png]]

Les filtres sont en fait un noyau d'image,qui est une petite matrice appliquée à une image entière.
Filtre flou : 
$$\begin{pmatrix}   
0.0625 & 0.125 & 0.0625 \\   
0.125 & 0.25 & 0.125 \\   
0.0625 & 0.125 & 0.0625   
\end{pmatrix}$$
Ils sont appliqués à toute l'image, comme ceci :

![[mask1.png]]

Puis, on le décale d'un pas qui est choisi par l'utilisateur.

Dans le contexte des CNNs, ces "filtres" sont appelés **noyaux de convolution**. Le processus qui consiste à les faire passer sur une image est connu sous le nom de **convolution**.

Pendant cette convolution, nous pourrions perdre les frontières, c'est pour quoi on peut rembourrer l'image avec plus de valeurs (les 0 autour) :

![[mask3x3.png]]

Cela va permettre de préserver la taille de l'image.

**Stride** : pas de déplacement du filtre
**Padding** : Ajouter une bordure exterieur de 0 pour prendre tous les pixels des bords

## Convolutions

Rappelons que l'utilisation d'un ANN pour l'ensemble des données du MNIST permet d'obtenir un réseau d'une exactitude relativement bonne. Cependant, le fait de toujours utiliser les modèles ANN pour les données d'images pose certains problèmes.

Ici nous avons 1 filtre, tous les neurones ne sont pas connectés. (image en 1D)

![[network1.png]]

Nous avons ici 2 filtres

![[network2.png]]

Mais, les images en niveaux de gris sont en 2D :

![[network4.png]]

![[network3.png]]

Pour les images en couleurs, on passe en 3D (les images sont considérées comme des tenseurs 3D composé de canaux de couleur rouge, verte, bleue. Chaque canal aura des valeurs d'intensité)

Exemple :

![[matrixImage.png]]

On utilise alors un filtre 3D pour chaque canal de couleur :

![[filterImage.png]]

Souvent, les couches de convolutions sont introduites dans une autre couche de convolution. Cela permet aux réseaux de découvrir des modèles à l'intérieur des modèles, généralement avec plus de complexité pour les couches de convolution ultérieures.

## Couches de Pooling

Même avec la connectivité locale, lorsqu'il s'agit d'images en couleur et éventuellement de dizaines ou de centaines de filtres, nous aurons une grande quantité de paramètres. Nous pouvons utiliser des couches de Pooling pour réduire ce phénomène.

Les couches de Pooling acceptent les couches convolutives comme entrée :

![[layers.png]]

Exemple, avec un max pooling, sur une fenêtre 2x2, et un pas de 2 :

![[maxPooling.png]]

On peut aussi le faire avec la moyenne, le minimum, ...

Cette couche de Pooling finira par supprimer beaucoup d'informations, même un petit "noyau" de Pooling de 2 par 2 avec un stride de 2 supprimera 75% des données d'entrée.

Une autre technique courante déployée avec un CNN est appelée "Dropout". Détaillée dans [[Overfitting et regularization]].


## Transfert learning

En partant d'un CNN pré entrainé sur un Gros dataset (ImageNet) on peut l'utiliser sur une base de données plus petite.

- Si notre base de données est assez **grande** et **similaire** à la grosse base, alors on effectue un Fine Tuning de toutes les couches. (beaucoup de données donc pas d'overfitting)

- Si notre base est **petite**, avec **beaucoup de différences** avec la grosse base, alors on peut faire de la **data augmentation** pour avoir plus de données ou alors entrainer un classifieur linéaire (SVM) sur des caractéristiques extraite des premières couches du réseau.

- Si notre base est **petite** et **similaire** à la grosse base, alors on extracte les caractéristique avec le réseau, et on utilise un classifieur linéaire (SVM)

- Si notre base est assez **grande** mais **différente** de la grosse base, on entraine le réseau from scratch.

De plus on peut aussi geler les premières couches du CNN (les Conv) pour garder les caractéristiques de bas niveaux. (pour le ResNet on gèle par bloc de couche résiduelle) 

> **Feature extractor** : On enlève les couches denses pour ne garder que la partie CNN, on passe nos images dedans et on récupère son vecteur descripteur à la dernière couche. But : utiliser le CNN comme extracteur de données

> **Fine-tuning** : On enlève les couches denses, et on les remplace par de nouvelles couches denses qui correspondent à nos données. (nombre de neurones en sortie). Puis on réentraine le réseau à partir du réseau entrainé sur la grosse base avec nos données. But : Adapter le CNN pour une nouvelle tâche


Voir aussi [[Transfert Learning en NLP]]


## UpSampling

- **Nearest Neighbor** : On remplie avec les pixels les plus proches
- **Bed of Nails** : On remplie de 0 les espaces vides
- **Max Unpooling**
- **Interpolation bilinéaire**
- **Convolution transposée** : La plus utilisée - produit scalaire avec les poids du filtre 


## Architectures

- **LeNet** : Le tout premier CNN. Suite de conv et pool, 60000 paramètres
- **AlexNet** : Premier à implémenter [[Fonctions d’activation#ReLU|ReLu]] dans un CNN, plusieurs conv Avant Pooling
- **LeNet-5**
- **DensNet** : On supprimes les paramètres (couches) qui sont inutiles (réduit le [[Vanishing & exploding Gradient#Vanishing gradient (disparition des gradients)|Vanishing Gradient]])
- **GoogLeNet**
- **ResNet** : Si trop de couches -> [[Vanishing & exploding Gradient#Vanishing gradient (disparition des gradients)|Vanishing Gradient]], donc on utilise des blocs résiduels 
- **VGG19** : kernel plus petit
- **MobileNet** : Plus léger pour les appareils mobiles (Google 2017)
- **NasNet** : Architecture AutoML pour trouver un meilleur modèle (Neural architecture search) -> par [[Intelligence artificielle/Reinforcement Learning/Introduction|Reinforcement Learning]] (taille filtres, strides, padding, ...)
- **EfficientNet** : On joue sur la profondeur (nb de couches), la largeur (taille des filtres) et la résolution (taille images)


Les CNN peuvent avoir tous types d'architectures !

![[layers2.png]]

![[layers3.png]]

![[layers4.png]]



## Cheat sheet

[[CNN_cs.png|CNN]]
[[famous_cnn.png|Famous CNNs]]

# Dataset MNIST (niveaux de gris)

(code)

# Dataset CIFAR-10 (Images en couleur)

(code)

# Travailler avec des fichiers d’images avec les CNNs

(code)

# CNNs pour la classification des cellules sanguines du paludisme/malaria

(code)