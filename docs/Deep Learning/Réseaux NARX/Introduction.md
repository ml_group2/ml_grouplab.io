
En anglais __Nonlinear Autoregressive models with eXogenous input__ 

Le modèle NARX permet la modélisation des systèmes non linéaires, notamment les Time series. Une des principales applications des réseaux de neurones dynamiques NARX concerne les systèmes de contrôle. 

L’intéret des réseaux NARX est son apprentissage plus efficace, dû aux descentes de gradients plus efficaces et aussi leur convergence plus rapide et une meilleure généralisation des données par rapport aux autres réseaux.

Certaines simulations montrent que les réseaux NARX sont souvent bien meilleurs pour découvrir les dépendances à long terme que les [[Reccurent neural network (RNN)]]. Une explication de la raison pour laquelle les retards de sortie peuvent aider les dépendances à long terme peut être trouvée en considérant la façon dont les gradients sont calculés en utilisant l'algorithme de rétropropagation à travers le temps ([[Backpropagation through time (BPTT)]]). Récemment, plusieurs études empiriques ont montré que lors de l'utilisation d'algorithmes d'apprentissage par descente de gradient, il pouvait être difficile d'apprendre un comportement temporel simple avec des dépendances temporelles longues, en d'autres termes il correspond aux problèmes pour lesquels la sortie d'un système à l'instant k dépend des entrées du réseau présentées aux temps r << k.  


### Liens 

[NARX for the Prediction of the Daily Direct Solar Radiation](https://hal.archives-ouvertes.fr/hal-01933133/document)
