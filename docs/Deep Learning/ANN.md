
[[#Modèle du perceptron]]
[[#Perceptron multi-couche]]
[[#Choix de la fonction d’activation de la dernière couche]]
[[#Fonction Coût et Descente de Gradient]]
[[#Backpropagation]]
[[#Implémentation]]


# Modèle du perceptron

Un perceptron est une forme de réseau de neurones introduite en 1958 par Frank Rosenblatt. 

Un énorme engouement naît, mais en 1969, Marvin Minsky et Seymour Papert publie un livre nommé “Perceptrons” dans lequel ils mentionnent les limites de ce que le perceptron peut faire.
Cela marque le début de ce qu’on appelle l’hiver de l’IA. 

![Capture d’écran 2022-03-22 à 16.40.33.png](neuronBio.png)

![Capture d’écran 2022-03-22 à 16.49.13.png](perceptronSimple.png)


# Perceptron multi-couche

Zhou Lu et plus tard Boris Hanin ont prouvé mathématiquement que les réseaux de neurones peuvent approximer n’importe quelle fonction continue convexe.

![Capture d’écran 2022-03-22 à 16.53.07.png](DeepNeuralNetwork.png)

Sur chaque neurone, on évalue une fonction d’activation sur la somme pondérée des entrées du neurone. Mais ces fonctions d’activation ont un sens pour une seule sortie. Que faire dans une situation multiclasse ? 

Il existe deux types de situations multiclasses :

- Classe non-exclusives
    - Un point de données peut se voir attribuer plusieurs classes/ catégories
        - exemple : les photos peuvent avoir plusieurs tags
- Classes mutuellement exclusives
    - Une seule classe par point de données
        - exemple : les photos peuvent être classées comme étant en niveaux de gris ou en couleur. Une photo ne peut pas être les deux à la fois

Organisation pour une classe de sortie :
![Organisation pour une classe de sortie](SimpleNeuralNetwork.png)

Organisation pour plusieurs classses :
![Organisation pour plusieurs classses](simpleDeepNeuralNetwork.png)


Organisation des classes multiples → Utilisation de **One-Hot encoding**

1. Classes mutuellement exclusives

![Capture d’écran 2022-03-22 à 17.18.35.png](OneHot.png)

2. Classes non-exclusives

![Capture d’écran 2022-03-22 à 17.19.12.png](OneHot2.png)

# Choix de la fonction d’activation de la dernière couche

- Pour les classes non exclusives : **Sigmoïde**
    
    ![Capture d’écran 2022-03-22 à 17.28.23.png](Activation.png)
    

- Pour les classes mutuellement exclusives : **Softmax**

$$
\sigma(z)_i =\frac{e^{z_i}}{\sum^K_{j=1}e^{z_j}} 
$$

pour $i=1, ..., K$z

Ce qui distribue les probabilité, et fait en sorte que la somme des sorties fasse 1.

Notes : [Fonctions d’activation](Fonctions%20d’activation.md) 

# Fonction Coût et Descente de Gradient

Notes : [[Fonction cout]]

Notes : [Descente de gradient](Descente%20de%20gradient.md) 

# Backpropagation

Le but est de savoir comment les résultats de la fonction coût changent par rapport aux pondérations dans le réseau, pour pouvoir les mettre à jour et minimiser la fonction coût.

Avec une représentation des couches $L_i$ comme ceci :

![Capture d’écran 2022-03-22 à 19.03.14.png](Intelligence%20artificielle/Deep%20Learning/images/BackProp.png)

Le but va être de calculer la dérivée de la fonction coût par rapport aux poids $w$, puis par rapport aux biais $b$ grâce à la règle des chaines :

Pour les poids de la couche $L$ : $\frac{\partial C_0}{\partial w^L}=\frac{\partial z^L}{\partial w^L}\frac{\partial a^L}{\partial z^L}\frac{\partial C_0}{\partial a^L}$ avec $z^L=w^La^{L-1}+b^L$, $a^L=\sigma(z^L)$ , $C_0(..) = (a^L-y)^2$

Pour les biais de la couche $L$ : $\frac{\partial C_0}{\partial b^L}=\frac{\partial z^L}{\partial b^L}\frac{\partial a^L}{\partial z^L}\frac{\partial C_0}{\partial a^L}$ avec $z^L=w^La^{L-1}+b^L$, $a^L=\sigma(z^L)$ , $C_0(..) = (a^L-y)^2$

Ainsi, on va pouvoir remonter dans le réseau et ajuster nos poids et biais afin de minimiser la sortie du vecteur d’erreur sur la dernière couche.

Notes : [[Back Propagation]]

# Implémentation

## Les bases de Keras
```python
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.offline import plot
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error
import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Activation

# Lecture à partir de Github pour pouvoir éxécuter le notebook sans se soucier d'importer le fichier csv
url = 'https://raw.githubusercontent.com/moncoachdata/DATA_DEEP_LEARNING/master/fake_reg.csv'
df = pd.read_csv(url)
df.head()

# Convertir Pandas en Numpy pour Keras
X = df[['feature1', 'feature2']].values
y = df['price'].values

# Split (séparation / répartition)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Normalisation et mise à l'échelle des données
scaler = MinMaxScaler()
# Remarque : pour éviter les fuites de données de l'ensemble de test, nous n'adaptons notre scaler qu'à l'ensemble d'entraînement
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# Création d'un modèle
model = Sequential()

model.add(Dense(4, activation='relu'))
model.add(Dense(4, activation='relu'))
model.add(Dense(4, activation='relu'))
# Couche finale pour notre prédiction avec un seul nœud de sortie
model.add(Dense(1))
model.compile(optimizer='rmsprop', loss='mse')


# Entrainement
model.fit(X_train, y_train, epochs=250)

# Évaluation
print(model.history.history)
loss = model.history.history['loss']
fig = go.Figure()
fig.add_scatter(x=list(range(len(loss))), y=loss)
plot(fig)

# Comparaison MSE test/train
print(model.metrics_names)
training_score = model.evaluate(X_train, y_train, verbose=0)
test_score = model.evaluate(X_test, y_test, verbose=0)
print(training_score), print(test_score)

# Prédiction
new_gem = [[998, 1000]]
new_gem = scaler.transform(new_gem)  # Pas oublier le Scale !
model.predict(new_gem)

# Sauvegarde et chargement d'un modèle
model.save('my_model.h5')
later_model = load_model('my_model.h5')
later_model.predict(new_gem)
```


#### Choix d'un optimiseur (optimizer) et perte (loss)

Gardez à l'esprit le type de problème que vous essayez de résoudre :

- Pour un problème de classification multi-classes :
```python
model.compile(
optimizer='rmsprop', 
loss='categorical_crossentropy',
metrics=['accuracy']
)
```

- Pour un problème de classification binaire :
```python
model.compile(
optimizer='rmsprop', 
loss='binary_crossentropy', 
metrics=['accuracy']
)
```

- Pour un problème de régression de l'erreur quadratique moyenne :
```python
model.compile(
optimizer='rmsprop', 
loss='mse'
)
```


## Régression avec Keras

