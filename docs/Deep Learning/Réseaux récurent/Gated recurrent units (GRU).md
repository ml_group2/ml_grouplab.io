Le RNN souffre du [[Vanishing & exploding Gradient]] et ne se souvient pas des états pendant très longtemps. Les GRUS sont une application de modules multiplicatifs qui tente de résoudre ces problèmes. C’est un exemple de réseau récurrent avec mémoire. La structure d’une unité GRU est présentée ci-dessous :

![[gru_unit.png]]
_GRU_


$$
\begin{align*}
z_t &= \sigma_g(W_zx_t+U_zh_{t-1}+b_z) \\ 
r_t &= \sigma_g(W_rx_t+U_rh_{t-1}+b_r) \\
h_t &= z_t⊙h_{t-1}+(1-z_t)⊙\phi_h(W_hx_t+U_h(r_t⊙h_{t-1})+b_h)\\
\end{align*}
$$

Où $⊙$ indique une multiplication élément par élément (produit Hadamard), $x_t$ est le vecteur d'entrée, $h_t$ est le vecteur de sortie, $z_t$ est le vecteur de mise à jour, $r_t$ est le vecteur de réinitialisation, $\phi_h$ est une tanh et $W$, $U$, $b$ sont des paramètres pouvant être appris.

Pour être précis, $z_t$ est un vecteur de porte qui détermine quelle part des informations passées doit être transmise dans la suite. Il applique une fonction sigmoïde à la somme de deux couches linéaires et un biais sur l’entrée $x_t$ et l’état précédent $h_{t−1}$.   
$z_t$ contient des coefficients entre 0 et 1 résultant de l’application de la fonction sigmoïde. L’état final de sortie $h_t$ est une combinaison convexe de $h_{t−1}$ et de $\phi_h(W_hx_t+U_h(rt⊙h_{t−1})+b_h)$ via $z_t$. Si le coefficient est égal à 1, la sortie de l’unité actuelle n’est qu’une copie de l’état précédent et ignore l’entrée (ce qui est le comportement par défaut). S’il est inférieur à 1, il prend en compte de nouvelles informations provenant de l’entrée.

La porte de réinitialisation $r_t$ est utilisée pour décider quelle quantité d’informations passées doit être oubliée. Dans le nouveau contenu de la mémoire $\phi_h(W_hx_t+U_h(r_t⊙h_{t−1})+b_h)$, si le coefficient dans $r_t$ est 0, alors il ne stocke aucune des informations du passé. Si en plus $z_t$ vaut 0, alors le système est complètement réinitialisé puisque $h_t$ ne regarderait que l’entrée.




### Ressources

[Understanding GRU Networks](https://towardsdatascience.com/understanding-gru-networks-2ef37df6c9be)
[Comprendre le fonctionnement d’un LSTM et d’un GRU en schémas](https://penseeartificielle.fr/comprendre-lstm-gru-fonctionnement-schema/)
