Sachez simplement que **les LSTMs sont construites pour mémoriser une partie des autres éléments d'une séquence**, et sont donc bien adaptées à des tâches traitant d'objets dont les éléments ont des dépendances entre eux à plus ou moins long terme, comme la traduction simultanée, qui ne peut pas se faire mot à mot, sans le contexte de ce qui a été prononcé avant.

Les GRUs sont en fait une version simplifiée des LSTMs qui ont été conçues beaucoup plus tôt par Hochreiter et Schmidhuber (1997). En constituant des cellules de mémoire pour préserver les informations passées, les LSTMs visent également à résoudre les problèmes de perte de mémoire à long terme des RNNs. La structure des LSTMs est présentée ci-dessous :

![[lstm_unit.png]]
_LSTM_

$$\begin{align*}
f_t&=\sigma_g(W_fx_t+U_fh_{t-1}+b_f)\\
i_t&=\sigma_g(W_ix_t+U_ih_{t-1}+b_i)\\
o_t&=\sigma_o(W_ox_t+U_oh_{t-1}+b_o)\\
c_t&=f_t⊙c_{t-1}+i_t⊙tanh(W_cx_t+U_ch_{t-1}+b_c)\\
h_t&=o_t⊙tanh(c_t)
\end{align*}
$$


Où $⊙$ indique une multiplication élément par élément, $x_t\in R^a$ est un vecteur d’entrée de l’unité LSTM, $f_t\in R^h$ est le vecteur d’activation de la porte d’oubli, $i_t\in R^h$ est le vecteur d’activation de la porte d’entrée/mise à jour, $o_t\in R^h$ est le vecteur d’activation de la porte de sortie, $h_t\in R^h$ est le vecteur d’état caché (également appelé sortie), $c_t\in R^h$ est le vecteur d’état de la cellule.

Une unité LSTM utilise un état de cellule $c_t$ pour transmettre l’information. Elle régule la manière dont l’information est préservée ou retirée de l’état de la cellule par des structures appelées _gates_ (portes). La porte d’oubli $f_t$ décide de la quantité d’informations que nous voulons conserver de l’état de cellule précédent $c_{t−1}$ en regardant l’entrée actuelle et l’état caché précédent. Elle produit un nombre entre 0 et 1 comme coefficient de $c_{t−1}$. $tanh⁡(W_cx_t+U_ch_{t−1}+b_c)$ calcule un nouveau candidat pour mettre à jour l’état de la cellule. Comme la porte d’oubli, la porte d’entrée $i_t$ décide de la part de mise à jour à appliquer. Enfin, la sortie $h_t$ est basée sur l’état de la cellule $c_t$, mais passe par une tanh⁡ puis est filtrée par la porte de sortie $o_t$.

Bien que les LSTMs soient largement utilisés en traitement du langage naturel, leur popularité est en baisse. Par exemple, la reconnaissance vocale se dirige vers l’utilisation de ConvNets temporels et les autres utilisations se dirigent vers l’utilisation de _transformers_.


### Ressources

[Understanding LSTM](https://arxiv.org/pdf/1909.09586v1.pdf)
[LSTM, Intelligence artificielle sur des données chronologiques](https://medium.com/smileinnovation/lstm-intelligence-artificielle-9d302c723eda)
[LSTM Recurrent Neural Networks — How to Teach a Network to Remember the Past](https://towardsdatascience.com/lstm-recurrent-neural-networks-how-to-teach-a-network-to-remember-the-past-55e54c2ff22e)
[Comprendre le fonctionnement d’un LSTM et d’un GRU en schémas](https://penseeartificielle.fr/comprendre-lstm-gru-fonctionnement-schema/)
Pages 273 à 279 de "L'apprentissage profond avec python"
