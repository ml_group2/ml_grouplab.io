
Pour faire du "sequence to sequence", i.e. de la traduction simultanée, ou du text-to-speech, ou encore du speech-to-text, l'état de l'art jusque ces dernières années, c'était les RNNs, nourris avec des séquences de word-embeddings, et parfois quelques couches de convolution sur les séquences d'entrée pour en extraire des caractéristiques (features) plus ou moins fines (fonction du nombre de couches), afin d'accélérer les calculs, avant de passer les infos au RNN.





Les ANNs sont soumis à l'explosion ou à l'évanouissement du gradient ([[Vanishing & exploding Gradient]]), et ce phénomène est d'autant plus prononcé qu'on traite de longue séquences ou qu'on les approfondis. De la même manière les RNNs y sont soumis quand on augmente le nombre d'étapes de traitement, mais de façon aggravée par rapport aux ANNs. En effet il peut y avoir des dizaines/centaines de mots dans une phrase, et rarement autant de couches dans un ANN. De plus, dans un ANN, chaque couche possède sa propre matrice de poids et ses propres fonctions d'activation. Les matrices peuvent parfois se contre-balancer les unes les autres et "compenser" ce phénomène. Dans un RNN avec une seule matrice les problèmes de gradient sont plus prononcés.
  
Afin de conserver la mémoire du contexte, et d'atténuer les problèmes de descente de gradient, les RNNs sont généralement composés d'unités un peu particulières, les [[Long short term memory (LSTM)]] (long short term memory), ou d'une de leur variantes les [[Gated recurrent units (GRU)]] (gated recurrent units). Il existe d'autres techniques pour les soucis de gradient dans les RNNs - gradient clipping quand ça explose, sauts de connexion, i.e. additive/concatenative skip/residual connexion quand ça "vanishe" etc... Voici un [article](https://www.slideshare.net/fgodin/skip-residual-and-densely-connected-rnn-architectures) pour plus de détails. 


## Ressources

Pages 265 à 273 de "L'apprentissage profond avec python"