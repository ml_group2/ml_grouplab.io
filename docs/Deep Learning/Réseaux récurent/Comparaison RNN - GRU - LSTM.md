




### Liens

[Medium article : RNN vs GRU vs LSTM](https://medium.com/analytics-vidhya/rnn-vs-gru-vs-lstm-863b0b7b1573)
[Tutorial on RNN | LSTM |GRU with Implementation](https://www.analyticsvidhya.com/blog/2022/01/tutorial-on-rnn-lstm-gru-with-implementation/)
[Tensorflow vs PyTorch for Text Classification using GRU](https://medium.com/swlh/tensorflow-vs-pytorch-for-text-classification-using-gru-e95f1b68fa2d)

[LSTM for text classification](https://www.kaggle.com/code/kredy10/simple-lstm-for-text-classification)
[RNN for text classification](https://www.tensorflow.org/text/tutorials/text_classification_rnn)
[Build a GRU RNN in Keras](https://pythonalgos.com/build-a-gru-rnn-in-keras/)
