- [Dynamic Neural Network Modelling of Soil Moisture Content for Predictive Irrigation Scheduling](https://www.mdpi.com/1424-8220/18/10/3408)

- [An ensemble of dynamic neural network identifiers for fault detection and isolation of gas turbine engines](https://www.sciencedirect.com/science/article/abs/pii/S0893608016000046)

- [A hybrid evolutionary dynamic neural network for stock market trend analysis and prediction using unscented Kalman filter](https://www.sciencedirect.com/science/article/abs/pii/S1568494614000593)

