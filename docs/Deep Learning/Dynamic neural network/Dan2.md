DAN2 est un type de réseau de neurones dynamiques en couches, crée en 2005 par M. Ghiassia, H. Saidaneb dans l'article [[DAN2_origin_2005.pdf]].

![[Capture d’écran 2022-04-28 à 16.58.54.png]]

On observe une succession de couche à 4 neurones. 
- Le neurone $C$ est un neurone de Constante
- Le neurone $F$ est un neurone qui contient les dépendances linéaire avec les données antérieures d’apprentissage : `current accumulated knowledge element = CAKE`  
- Les 2 autres neurones $H$ et $G$ contiennent des résidus non linéaires capturé par une fonction de transfert en utilisant une somme pondérée et normalisée des variables d'entrée : `current residual nonlinear element = CURNOLE`

Le nombre de couches et défini soit à l'avance, soit correspond au dépassement d'un seuil de l'accuracy.