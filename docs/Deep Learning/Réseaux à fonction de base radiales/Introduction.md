
__En anglais Radial basis network (RBN)__

Ce sont des réseaux de type _feedforward_, avec 1 seule couche cachée.
La particularité de ces réseaux réside dans le fait qu’ils sont capables de fournir une représentation locale de l'espace grâce à des fonctions de base radiales φ(|| . ||) dont l'influence est restreinte à une certaine zone de cet espace. ( ||.|| représente la norme euclidienne) 

La couche d'entrée peut être représentée par un vecteur de $R^n$ 
La fonction d'activation non linéaire utilisée pour la couche cachée est une Gaussienne qui décide de l'activation du neurone en se basant sur la distance entre les données d'entrée et les centres des Gaussiennes. La dernière couche, utilise une fonction linéaire.

**Utilisations :** Approximation de fonction, prédiction sur les time series, classification et "system control"

Exemple réseau à fonctions de base radiales :

![[RFR.png]]


## Liens

[Github code](https://github.com/JuliaSzymanska/Artificial-Intelligence/tree/master/RBF)
