
En français traitement automatique du langage, le NLP est une discipline qui porte essentiellement sur la **compréhension**, la **manipulation** et la **génération** du **langage naturel par les machines**. Il nécessite une grande quantité de données. Il permet de créer des chatbot, classifier des tweets, faire de l'analyse de sentiments, créer des assistants vocaux (speechTOtext, textTOspeech), faire de la traduction. Les modèles utilisés sont les réseaux récurents ([[Reccurent neural network (RNN)|RNN]], [[Long short term memory (LSTM)|LSTM]] et [[Gated recurrent units (GRU)|GRU]]) ainsi que les [[Intelligence artificielle/Deep Learning/Transformers/Introduction|Transformers]].


#### Nettoyage de données text
- Suppression **des signes de ponctuation inutiles, emojis, url** car pas de valeur ajoutée
- **Tokenisation** : séparation de la chaîne de texte dans une liste de mots/groupes nominaux
- **Stemming** : Supprimer les affixes de mots (conjuguaison, pluriel/féminin) pour ne garder que la racine
- **Part of speech** (POS): étiquetage de chaque mot (adjectif, nom, verbe)
- **Lemmatisation** : isoler la forme canonique des mots à partir d'un dictionnaire
- **Suppression des stopwords** 




### Liens

[NLP notes](https://ia-z.github.io/ia-z/docs/NLP/chapitre1_introduction/2_DonneesTextuelles.html)
[Cours ensae](https://nlp-ensae.github.io)
https://datascientest.com/introduction-au-nlp-natural-language-processing
[DeepLearning.AI](https://www.deeplearning.ai/resources/natural-language-processing/?utm_campaign=The%20Batch&utm_medium=email&_hsmi=234209050&_hsenc=p2ANqtz-_hHEd2SCo2aie2OyZ8wuALkg9B41BvD8Ft5o_p6ZpmwVLSahOPomea0fiQSAXtMe7nJOgn-OFe1C1H8lRogj-8uv_s3H1Tbixsajl7gLjNguqafGk&utm_content=234208318&utm_source=hs_email)
