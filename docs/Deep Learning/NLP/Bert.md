
**BERT** ou encore **B**idirectional **E**ncoder **R**epresentations from **T**ransformers est un modèle de représentation de textes écrit en langage naturel. La représentation faite par BERT à la particularité d’être contextuelle. C’est-à-dire qu’un mot n’est pas représenté de façon statique comme dans un embedding classique mais en fonction du sens du mot dans le contexte du texte. Par exemple, le mot « baguette » aura des représentations différentes dans « la baguette du magicien » et « la baguette du boulanger ». En plus, le contexte de BERT est bidirectionel, c’est-à-dire que la représentation d’un mot fait intervenir à la fois les mots qui le précèdent et les mots qui le suivent dans une phrase.

Le principe d’utilisation de BERT est tout simple : _Il est « déjà » pré-entraîné sur une grande quantité de données, on le modifie pour une tâche précise puis on le (re)entraîne avec nos propres données._ La modification dont il est question ici consiste en général à rajouter un réseau de neurones à la sortie de BERT. Cela s’appelle : _**le fine-tuning.**_


Il a l'avantage par rapport à ses concurrents [Open AI GTP](https://openai.com/blog/language-unsupervised/) _(_[_GPT-2_](https://openai.com/blog/better-language-models/) _est ici pour ceux que ça intéresse)_ et [ELMo](https://allennlp.org/elmo) d'être bidirectionnel, il n'est pas obligé de ne regarder qu'en arrière comme OpenAI GPT ou de concaténer la vue "arrière" et la vue "avant" entraînées indépendamment comme pour ELMo.


> **Bert** vous permettra par exemple de **classifier les tweets selon le sentiment** qu’ils renvoient ou encore de **créer un assistant virtuel** capable de répondre aux questions de façon intelligente.


## Architecture

BERT réutilise l’architecture des [Transfomers](https://ledatascientist.com/a-la-decouverte-du-transformer/) (d’où le « T » de BERT). En effet, BERT n’est rien d’autre qu’une superposition d’_encoders_.

![Architecture BERT](https://i0.wp.com/ledatascientist.com/wp-content/uploads/2020/11/archi_bert.png?resize=532%2C439&ssl=1)

BERT-Base

Les encodeurs ont tous la même structure (voir cet [article](https://ledatascientist.com/a-la-decouverte-du-transformer/)) mais ne partagent pas les mêmes poids.

La version représentée ci-dessus est la version dite « Base » de BERT. Elle est constituée de 12 encoders. Il existe une version plus grande dite « Large » qui a 24 encodeurs. Évidemment la version large est plus performante mais plus gourmande en ressource machine.

Le modèle a 512 entrées qui correspondent chacune à un token. La première entrée correspond à un token spécial le “[CLS]” pour “classification” et qui permet d’utiliser BERT pour une tâche de classification de texte.

Il a également 512 sorties de taille 768 chacune (1024 pour la version Base). Le premier vecteur correspond au vecteur de classification.

La sortie de chacun des 12 encodeurs peut être considérée comme une représentation vectorielle de la séquence passée en entrée. La pertinance de cette représentation est assurée par le mécanisme d**‘attention** mis en œuvre par les encodeurs.

L’innovation qu’apporte BERT n’est pas dans son architecture. Vu qu’on vient de voir que la seule pièce qui le compose a été gentiment empruntée au Transformer. Et puis, OpenAI avec son [GPT](https://openai.com/blog/language-unsupervised/) avait déjà un modèle pré-entraîné basé sur les Transformers. Qu’est-ce que BERT apporte comme nouveauté concrètement ?


## Les dérivés de Bert

Plusieurs modèles réutilisant **BERT** ont vu le jour ces dernières années. Dans la plupart des cas l’architecture reste la même mais l’entraînement change sensiblement. **RoBERTa**, par exemple, est une « version de **BERT** » avec des paramètres de pré-entraînement différents et la tâche de NSP supprimée. Cela dans le but de rendre le pré-entraînement plus rapide. En ce qui concerne la francophonie, nous avons [**CamemBERT**](https://camembert-model.fr/) qui est basée sur RoBERTa et qui est entraîné sur un jeu de données en français.

