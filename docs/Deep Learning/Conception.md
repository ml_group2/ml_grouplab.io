Cette note est dédiée au choix de l'architecture d'un ANN, de la fonction de perte, des activations, de l'optimiseur, etc.


## Processus

1. Définissez votre problème et les données sur lesquelles vous entraînerez le modèle. Recueillez des données, éventuellement annotez-les avec des étiquettes si nécessaire.

2. Choisissez comment vous allez mesurer le succès de votre problème. Quelles métriques allez-vous surveiller sur vos données de validation ?

3. Déterminez votre protocole d'évaluation : validation simple (hold-out validation) ? Validation croisée K-fold (k-fold validation) ? Quelle fraction des données devez-vous utiliser pour la validation ?

4. Développez un premier modèle qui fonctionne mieux qu'un modèle de base : c'est-à-dire un modèle doté d'une puissance statistique.

5. Développez un modèle qui sur-ajuste.

6. Régularisez votre modèle et réglez ses hyper-paramètres en fonction de ses performances sur les données de validation. Un grand nombre de recherches sur l'apprentissage automatique ont tendance à se concentrer uniquement sur cette dernière étape - mais il est important de toujours garder une vue d'ensemble.


## Architectures clés des réseaux

Les trois familles d'architectures de réseau que vous devriez connaître sont les **réseaux entièrement connectés** (densely connected networks), les **réseaux convolutifs** (convolutional networks) et les **réseaux récurrents** (recurrent networks). 
Chaque type de réseau est destiné à une modalité d'entrée spécifique : une architecture de réseau (entièrement connectée, de convolution, récurrente) code des hypothèses sur la structure des données : un espace d'hypothèses (hypothesis space) dans lequel la recherche d'un bon modèle se poursuivra. Le fait qu'une architecture donnée soit performante ou non sur un problème donné dépend entièrement de la correspondance entre la structure des données et les hypothèses de l'architecture de réseau.

Ces différents types de réseaux peuvent être facilement combinés pour obtenir des réseaux multimodaux plus grands, tout comme vous assembleriez des briques LEGO.
D'une certaine manière, les couches d'apprentissage profond sont les briques LEGO du traitement de l'information. 

Voici un aperçu rapide de correspondances entre les modalités d'entrées et les architectures de réseau appropriées :

|Modalités d'entrées|architectures réseau appropriées|
|---|---|
|**données vectorielles**|réseau entièrement connecté (couches Dense)|
|**données d'images**|ConvNet 2D|
|**données sonores (par exemple, sous la forme d'ondes)**|soit de préférence des ConvNet 1D ou des RNN|
|**données textuelles**|soit de préférence des ConvNet 1D ou des RNN|
|**données de séries temporelles**|soit de préférence des RNN ou des ConvNet 1D|
|**autres types de données séquentielles**|soit des RNN ou des ConvNet 1D. Préférez les RNN si le classement des données est très significatif (par exemple, comme pour les séries temporelles, mais pas pour le texte)|
|**données vidéo**|soit des ConvNet 3D (si vous devez capturer des effets de mouvement), soit une combinaison d'un ConvNet 2D pour l'extraction de caractéristiques suivie, au choix, d'un RNN ou d'un ConvNet 1D pour traiter les séquences issues du premier traitement|
|**données volumétriques**|ConvNet 3D|



## Exemples architectures simples

![[simple_archi.png]]

De gauche à droite :  un neurone artificiel seul **(A)**, des perceptrons simples avec une couche d'entrée et une couche de sortie **(B et C)**, un perceptron multicouche doté d'une couche d'entrée, d'une couche cachée et d'une couche de sortie. L'augmentation du nombre de couches et de neurones entraine une augmentation du pouvoir de séparation.


## Fonction de perte

Fiche -> [[Fonction cout]]

## Optimiseur (optimizer)

Il détermine comment le réseau sera mis à jour en fonction de la perte. Dans la plupart des cas, il est raisonnable d'utiliser `rmsprop` et son taux d'apprentissage par défaut. Il existe aussi le [[Batch, Mini Batch & Stochastic Gradient Descent|SGD]].

- **RMSprop** : (root mean squared propagation) Moyenne mobile du carré des gradients pour chaque poids, puis division du gradient par la racine de sa moyenne
- **Adam** : méthode de descente de gradient stochastique qui repose sur l'estimation adaptative des moments de premier et de second ordre.


## Fonction d'activation

Fiche -> [[Fonctions d’activation]]


## Dégradation du learning rate

- Inverse (timed-based)
- exponential decay
- step decay

## Hyperparamètres et données

- Ne pas donner trop de données dans le **train set** sous peine d'overfitting
- Ne pas prendre un nombre d'**epochs** trop petit pour ne pas avoir de mauvaises performances
- Ne pas prendre un **batch** trop petit pour ne pas ralentir l'apprentissage mais pas trop grand pour ne pas altérer les performances du modèle

