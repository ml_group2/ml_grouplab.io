# La **class** de base


```python
model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(8))
model.add(tf.keras.layers.Dense(1))
model.compile(optimizer='sgd', loss='mse')
# This builds the model for the first time:
model.fit(x, y, batch_size=32, epochs=10)
```

## Méthodes

#### 1) add

```python
Sequential.add(layer)
```

Ajoute un objet de type layer sur la pile de couches. 

#### 2) pop

```python
Sequential.pop()
```

Supprime la dernière couche du modèle.

#### 4) compile

```python
Model.compile(
    optimizer="rmsprop",
    loss=None,
    metrics=None,
    loss_weights=None,
    weighted_metrics=None,
    run_eagerly=None,
    steps_per_execution=None,
    jit_compile=None,
    **kwargs
)
```

Configure le modèle pour l’entraînement.

- **optimizer** : Nom de l'optimiseur ou instance de `tf.keras.optimizers`
- **loss** : fonction de perte. Nom de la fonction ou instance de `tf.keras.Loss`. C'est une fonction de signature `f(y_true, y_pred)`
- **metrics** : Liste de métriques pour évaluer le modèle durant l’entraînement et le test. Utilisation du nom de la métrique ou de l'instance de `tf.keras.metrics.Metric`

#### 5) fit

```python
Model.fit(
    x=None,
    y=None,
    batch_size=None,
    epochs=1,
    verbose="auto",
    callbacks=None,
    validation_split=0.0,
    validation_data=None,
    shuffle=True,
    class_weight=None,
    sample_weight=None,
    initial_epoch=0,
    steps_per_epoch=None,
    validation_steps=None,
    validation_batch_size=None,
    validation_freq=1,
    max_queue_size=10,
    workers=1,
    use_multiprocessing=False,
)
```

Entrainer le modèle pour un nombre fixe d'epochs. 

- **x** : Données d'entrées de types Numpy array, list of arrays, Tensorflow tensor, list of tensors, dict qui est mappé sur le nom des inputs si les entrées sont nommées, tf.data dataset, un générateur ou objet `keras.utils.Sequence`.
- **y** : Target. 
- **batch_size** : integer ou None. Nombre de données pour la mise à jour du gradient. Par défaut à 32. Ne pas mettre ce paramètre si les données sont un dataset, un generateur ou un objet `keras.utils.Sequence`
- **epochs** : Entier. Nombre d'epochs pour enrainer le modèle. C'est le nombre de fois que l'on itère sur les données `x` et `y`. 
- **verbose** : 0 pour silencieux, 1 pour la barre de progression, ou 2 poour une ligne par epoch. 


Cette méthode retourn un objet `History.history` qui contient l’historique des coûts lors de l’entraînement et des métriques pour chaque epoch. 

#### 6) evaluate

```python
Model.evaluate(
    x=None,
    y=None,
    batch_size=None,
    verbose="auto",
    sample_weight=None,
    steps=None,
    callbacks=None,
    max_queue_size=10,
    workers=1,
    use_multiprocessing=False,
    return_dict=False,
    **kwargs
)
```

Retourne le cout et les métriques du modèle lors de la phase de test. 

- **x** : Données d'entrées de type Numpy array, tensorflow tensor, dict, tf.data
- **y** : Target. 
- **batch_size** : Entier ou None. Nombre de données par batch. Défaut à 32
- **verbose** : 0 pour silencieux, 1 pour la barre de progression, 2 pour une ligne simple. 

Cette méthode retourne le coût du modèle lors de la phase de test (scalaire dans le cas ou le modèle a une seule sortie, sinon une liste de scalaires) 

#### 7) predict

```python
Model.predict(
    x,
    batch_size=None,
    verbose="auto",
    steps=None,
    callbacks=None,
    max_queue_size=10,
    workers=1,
    use_multiprocessing=False,
)
```

Génére les prédictions grâce aux données d'entrées.

- **x** : Données d'entrées de type Numpy array, tensorflow tensor, list of tensors, tf.data
- **batch_size** : Entier ou None. Nombre de données par batch.
- **verbose** : 0 pour silencieux, 1 pour la barre de progression, 2 pour une ligne simple. 

Cette méthode retourne les prédictions sous forme de tableau(x) Numpy.

