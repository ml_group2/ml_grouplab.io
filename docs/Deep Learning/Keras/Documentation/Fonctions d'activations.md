## Utilisation

Les activations peuvent être utilisées avec une couche `Activation` ou avec l'argument `activation` des couches "forward" :

```python
model.add(layers.Dense(64, activation=activations.relu))
```

C'est équivalent à :

```python
from tensorflow.keras import layers 
from tensorflow.keras import activations  

model.add(layers.Dense(64)) model.add(layers.Activation(activations.relu))
```

Toutes les activations déjà implémentées peuvent être renseignées avec simplement leur nom :

```python
model.add(layers.Dense(64, activation='relu'))
```



## Fonctions disponibles

Info fonction d'activation : [[Fonctions d’activation]]

#### 1) relu

```python
tf.keras.activations.relu(x, 
						  alpha=0.0, 
						  max_value=None, 
						  threshold=0.0)
```

- **x** : tenseur ou variable
- **alpha** : float qui gère la pente pour les valeurs inférieurs au seuil
- **max_value** : float qui donne la plus grande valeur que la fonction va retourner. 
- **threshold** : float qui correspond au seuil d'activation du neurone


#### 2) sigmoid

```python
tf.keras.activations.sigmoid(x)
```

- **x** : Tenseur d'entrée


#### 3) softmax

```python
tf.keras.activations.softmax(x, axis=-1)
```

- **x** : tenseur d'entrée
- **axis** : Entier qui correspond à l'axe sur lequel la normalisation softmaw est appliquée


#### 4) softplus

```python
tf.keras.activations.softplus(x)
```

- **x** : tenseur d'entrée


#### 5) softsign

```python
tf.keras.activations.softsign(x)
```

- **x** : tenseur d'entrée


#### 6) tanh

```python
tf.keras.activations.tanh(x)
```

- **x** : tenseur d'entrée


#### 7) selu 

```python
tf.keras.activations.selu(x)
```

- **x** : tenseur ou variable d'entrée


#### 8) elu 

```python
tf.keras.activations.elu(x, alpha=1.0)
```

- **x** : tenseur d'entrée
- **alpha** : scalaire qui controle la valeur pour laquelle la fonction sature pour les valeurs négatives


#### 9) exponential

```python
tf.keras.activations.exponential(x)
```

- **x** : tenseur d'entrée

