```python
# fonction pour creer le modele keras
def define_model(init_mode='uniform', activation='relu', neurons=10, optimizer='adam'):
	# creer le modele

	model = Sequential()

	model.add(Dense(8, input_dim=8, 
	activation=activation,kernel_initializer=init_mode))

	model.add(Dense(neurons,
	activation=activation,kernel_initializer=init_mode))

	model.add(Dense(1, activation='sigmoid'))

	model.compile(loss='binary_crossentropy', 
	optimizer=optimizer,metrics=['accuracy'])

	return model

  

from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV

# creer le modele
model = KerasClassifier(build_fn=define_model, epochs=10, batch_size=10, verbose=0)

  
# parametres de la grille de recherche
init_modes = ['uniform', 'normal']
activations = ['relu', 'sigmoid', 'linear']
neurons = [1, 5, 10, 15]
optimizers = ['SGD', 'Adam']
grid_param = dict(init_mode=init_modes, activation=activations, neurons=neurons, optimizer=optimizers)


grid = GridSearchCV(estimator=model, param_grid=grid_param, n_jobs=-1, cv=3)

grid_result = grid.fit(X, Y)

# afficher les resultats
print("Best: %f with %s" % (grid_result.best_score_, grid_result.best_params_))
```