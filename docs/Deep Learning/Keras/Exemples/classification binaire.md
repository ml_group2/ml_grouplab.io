### Classification des critiques de films

```python
from keras.datasets import imdb  
import numpy as np  
from keras import models, layers, losses, optimizers, metrics  
import plotly.graph_objects as go  
from plotly.offline import plot  
```

Le paramètre `num_words` sert à conserver les 10000 mots les plus fréquents. Chaque critique est une liste d'entiers correspondant à un mot dans un dictionnaire de mots .

```python
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=10000)  
  
# recreer une critique  
word_index = imdb.get_word_index()  
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])  
decoded_review = ' '.join([reverse_word_index.get(i - 3, '?') for i in train_data[0]])  
  
  
# vectorisation  
def vectorize_sequences(sequences, dimension=10000):  
    # création d'une matrice ne contenant que des zéros et de la forme  
    # (len(sequences), dimension)    results = np.zeros((len(sequences), dimension))  
  
    # init à 1 des valeurs correspondantes aux indices spécifiques de results[i]  
    for i, sequence in enumerate(sequences):  
        results[i, sequence] = 1  
    return results  
  
  
# vectorisation des données d'apprentissage  
x_train = vectorize_sequences(train_data)  
# vectorisation des données de test  
x_test = vectorize_sequences(test_data)  
  
# transformation des étiquettes en vecteurs  
y_train = np.asarray(train_labels).astype('float32')  
y_test = np.asarray(test_labels).astype('float32')  
  
  
# modèle  
model = models.Sequential()  
model.add(layers.Dense(16, activation='relu', input_shape=(10000,)))  
model.add(layers.Dense(16, activation='relu'))  
model.add(layers.Dense(1, activation='sigmoid'))  
```

Choix de la fonction de perte et de l'optimiseur :
- classification binaire avec une sortie unique qui correspond à une probabilité donc on choisi la fonction de perte `binary_crossentropy`.
- on peut aussi prendre `mean_squared_error` mais `binary_crossentropy` mieux car on  a en sortie une probabilité.

```python
# compilation  
model.compile(optimizer=optimizers.RMSprop(learning_rate=0.001),  
              loss=losses.binary_crossentropy,  
              metrics=[metrics.binary_accuracy])  
  
# création d'un ensemble de validation  
x_val = x_train[:10000]  
partial_x_train = x_train[10000:]  
  
y_val = y_train[:10000]  
partial_y_train = y_train[10000:]  
  
# entrainement  
history = model.fit(partial_x_train,  
                    partial_y_train,  
                    epochs=20,  
                    batch_size=512,  
                    validation_data=(x_val, y_val))  
```

```python
# tracé de la perte  
history_dict = history.history  
loss_values = history_dict['loss']  
val_loss_values = history_dict['val_loss']  
  
epochs = [i for i in range(1, 20)]  
  
fig = go.Figure()  
  
fig.add_scatter(x=epochs, y=loss_values, mode='markers', marker=dict(color='blue'), name='Entrainement')  
fig.add_scatter(x=epochs, y=val_loss_values, mode='lines', marker=dict(color='blue'), name='Validation')  
  
fig.update_layout(title="Perte pendant l'entrainement et la validation", )  
fig.update_xaxes(title_text="Nombre d'epochs")  
fig.update_yaxes(title_text="Perte")  
plot(fig)  
```

![[Loss_1.png]]

```python
# tracé de l'accuracy  
acc_values = history_dict['binary_accuracy']  
val_acc_values = history_dict['val_binary_accuracy']  
  
fig = go.Figure()  
  
fig.add_scatter(x=epochs, y=acc_values, mode='markers', marker=dict(color='blue'), name='Entrainement')  
fig.add_scatter(x=epochs, y=val_acc_values, mode='lines', marker=dict(color='blue'), name='Validation')  
  
fig.update_layout(title="Accuracy pendant l'entrainement et la validation", )  
fig.update_xaxes(title_text="Nombre d'epochs")  
fig.update_yaxes(title_text="Accuracy")  
plot(fig)

```

![[Accuracy_1.png]]
